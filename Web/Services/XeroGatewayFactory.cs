﻿using AIYP.Gateways.Xero;
using AIYP.Gateways.Xero.Services;
using Microsoft.Extensions.Options;
using System.Security.Cryptography.X509Certificates;

namespace AIYP.Web.Services
{
    /// <summary>
    /// Xero gateway factory.
    /// </summary>
    public class XeroGatewayFactory : IXeroGatewayFactory
    {
        private readonly XeroGatewaySettings settings;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="settingsOptions"></param>
        public XeroGatewayFactory(IOptions<XeroGatewaySettings> settingsOptions)
        {
            this.settings = settingsOptions.Value;
        }

        /// <summary>
        /// Returns Xero gateway.
        /// </summary>
        /// <returns>Xero gateway.</returns>
        public IXeroGateway Create()
        {
            var authHeaderFactory = new OAuthAuthorizationHeaderFactory(CreateSignatureService());
            return new XeroGateway(authHeaderFactory, settings);
        }

        private ISignatureService CreateSignatureService()
        {
            var certificate = new X509Certificate2(
                this.settings.CertificatePath, 
                this.settings.CertificatePassword, 
                X509KeyStorageFlags.MachineKeySet);

            return new PrivateSignatureService(certificate);
        }
    }
}
