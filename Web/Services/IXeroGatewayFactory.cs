﻿using AIYP.Gateways.Xero;

namespace AIYP.Web.Services
{
    /// <summary>
    /// Xero gateway factory interface.
    /// </summary>
    public interface IXeroGatewayFactory
    {
        /// <summary>
        /// Returns Xero gateway.
        /// </summary>
        /// <returns>Xero gateway.</returns>
        IXeroGateway Create();
    }
}
