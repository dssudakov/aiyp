﻿using AIYP.Integrations.Xero;
using AIYP.Web.Models;
using AIYP.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace AIYP.Web.Controllers
{
    /// <summary>
    /// Home controller.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly IXeroGatewayFactory xeroGatewayFactory;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="xeroGatewayFactory">Xero gateway factory.</param>
        public HomeController(IXeroGatewayFactory xeroGatewayFactory)
        {
            this.xeroGatewayFactory = xeroGatewayFactory;
        }

        /// <summary>
        /// Index action.
        /// </summary>
        /// <returns>View with Xero data model.</returns>
        public async Task<IActionResult> Index()
        {
            var gateway = xeroGatewayFactory.Create();
            var dataService = new XeroService(gateway);
            var data = await dataService.GetDataAsync(DateTime.Now.AddDays(-60), null);

            return View(data);
        }

        /// <summary>
        /// Returns error view.
        /// </summary>
        /// <returns>Error view.</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
