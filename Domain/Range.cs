﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Range.cs" company="">
//   
// </copyright>
// <summary>
//   The range.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP
{
    /// <summary>
    ///     The range.
    /// </summary>
    public class Range
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Range"/> class.
        /// </summary>
        /// <param name="low">
        /// The low.
        /// </param>
        /// <param name="high">
        /// The high.
        /// </param>
        public Range(decimal low, decimal high)
        {
            if (low == -1)
            {
                this.Low = null;
            }
            else
            {
                this.Low = low;
            }

            if (high == -1)
            {
                this.High = null;
            }
            else
            {
                this.High = high;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Range"/> class.
        /// </summary>
        /// <param name="low">
        /// The low.
        /// </param>
        public Range(decimal low)
        {
            this.Low = low;
            this.High = null;
        }

        /// <summary>
        ///     Gets or sets the high.
        /// </summary>
        public decimal? High { get; set; }

        /// <summary>
        ///     Gets or sets the low.
        /// </summary>
        public decimal? Low { get; set; }

        /// <summary>
        ///     The is null.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool IsNull()
        {
            return this.High == null || this.Low == null;
        }
    }
}