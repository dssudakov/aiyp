// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SummaryType.cs" company="">
//   
// </copyright>
// <summary>
//   The summary type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP
{
    /// <summary>
    ///     The summary type.
    /// </summary>
    public enum SummaryType
    {
        /// <summary>
        ///     The gross profit margin.
        /// </summary>
        GrossProfitMargin, 

        /// <summary>
        ///     The net profit margin.
        /// </summary>
        NetProfitMargin, 

        /// <summary>
        ///     The roi.
        /// </summary>
        ROI, 

        /// <summary>
        ///     The average day debtor.
        /// </summary>
        AverageDayDebtor, 

        /// <summary>
        ///     The average day creditor.
        /// </summary>
        AverageDayCreditor, 

        /// <summary>
        ///     The short term cash forecast.
        /// </summary>
        ShortTermCashForecast, 

        /// <summary>
        ///     The current asset to current liability.
        /// </summary>
        CurrentAssetToCurrentLiability, 

        /// <summary>
        ///     The total assets to liability.
        /// </summary>
        TotalAssetsToLiability, 

        /// <summary>
        ///     The sales.
        /// </summary>
        Sales, 

        /// <summary>
        ///     The rent.
        /// </summary>
        Rent, 

        /// <summary>
        ///     The labour.
        /// </summary>
        Labour, 

        /// <summary>
        ///     The expenses.
        /// </summary>
        Expenses
    }
}