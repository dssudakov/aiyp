﻿using AIYP.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using XeroMessages = AIYP.Gateways.Xero.Messages;

namespace AIYP.Integrations.Xero
{
    /// <summary>
    /// Xero contacts extensions.
    /// </summary>
    static class XeroContactExtensions
    {
        /// <summary>
        /// Converts Xero contacts into contacts.
        /// </summary>
        /// <param name="xeroContacts">Xero contacts.</param>
        /// <returns>Contacts.</returns>
        public static IEnumerable<Contact> ToContacts(this IEnumerable<XeroMessages.Contact> xeroContacts)
        {
            if (xeroContacts == null)
            {
                throw new ArgumentNullException(nameof(xeroContacts));
            }

            return xeroContacts.Select(x => x.ToContact());
        }

        /// <summary>
        /// Converts Xero contact into contact.
        /// </summary>
        /// <param name="xeroContact">Xero contacts.</param>
        /// <returns>Contacts.</returns>
        public static Contact ToContact(this XeroMessages.Contact xeroContact)
        {
            if (xeroContact == null)
            {
                throw new ArgumentNullException(nameof(xeroContact));
            }

            return new Contact
            {
                ABN = xeroContact.TaxNumber,
                CompanyName = xeroContact.Name,
                FirstName = xeroContact.FirstName,
                LastName = xeroContact.LastName
            };
        }
    }
}
