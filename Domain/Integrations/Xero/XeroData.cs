﻿using AIYP.Accounting;
using System.Collections.Generic;

namespace AIYP.Integrations.Xero
{
    /// <summary>
    /// Xero data.
    /// </summary>
    public class XeroData
    {
        /// <summary>
        /// Accounts.
        /// </summary>
        public IList<Account> Accounts { get; set; }

        /// <summary>
        /// Contacts.
        /// </summary>
        public IList<Contact> Contacts { get; set; }

        /// <summary>
        /// Invoices.
        /// </summary>
        public IList<Invoice> Invoices { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public XeroData()
        {
            Accounts = new List<Account>();
            Contacts = new List<Contact>();
            Invoices = new List<Invoice>();
        }
    }
}
