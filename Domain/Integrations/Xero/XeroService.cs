﻿using AIYP.Gateways.Xero;
using AIYP.Gateways.Xero.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIYP.Integrations.Xero
{
    /// <summary>
    /// Xero integration service.
    /// </summary>
    public class XeroService : IXeroService
    {
        private readonly IXeroGateway gateway;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="gateway">Xero gateway.</param>
        public XeroService(IXeroGateway gateway)
        {
            this.gateway = gateway;
        }

        /// <summary>
        /// Returns Xero data.
        /// </summary>
        /// <param name="startDate">Start date.</param>
        /// <param name="endDate">End date.</param>
        /// <returns>Xero data.</returns>
        public async Task<XeroData> GetDataAsync(DateTime startDate, DateTime? endDate)
        {
            var xeroAccounts = await GetXeroAccountsAsync(startDate, endDate);
            var xeroContacts = await GetXeroContactsAsync(startDate, endDate);
            var xeroInvoices = await GetXeroInvoicesAsync(startDate, endDate);

            return new XeroData
            {
                Accounts = xeroAccounts.ToAccounts().ToList(),
                Contacts = xeroContacts.ToContacts().ToList(),
                Invoices = xeroInvoices.ToInvoices().ToList()
            };
        }


        private async Task<IEnumerable<Account>> GetXeroAccountsAsync(DateTime startDate, DateTime? endDate)
        {
            var request = new GetAccountsRequest
            {
                StartDate = startDate,
                EndDate = endDate
            };
            var response = await this.gateway.SendAsync(request);

            return response?.Accounts;
        }

        private async Task<IEnumerable<Contact>> GetXeroContactsAsync(DateTime startDate, DateTime? endDate)
        {
            var request = new GetContactsRequest
            {
                StartDate = startDate,
                EndDate = endDate
            };
            var response = await this.gateway.SendAsync(request);

            return response?.Contacts;
        }


        private async Task<IEnumerable<Invoice>> GetXeroInvoicesAsync(DateTime startDate, DateTime? endDate)
        {
            var request = new GetInvoicesRequest
            {
                StartDate = startDate,
                EndDate = endDate
            };
            var response = await this.gateway.SendAsync(request);

            return response?.Invoices;
        }
    }
}
