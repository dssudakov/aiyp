﻿using AIYP.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using XeroMessages = AIYP.Gateways.Xero.Messages;

namespace AIYP.Integrations.Xero
{
    /// <summary>
    /// Xero accounts extensions.
    /// </summary>
    static class XeroAccountExtensions
    {
        /// <summary>
        /// Converts Xero accounts into accounts.
        /// </summary>
        /// <param name="xeroAccounts">Xero accounts.</param>
        /// <returns>Accounts.</returns>
        public static IEnumerable<Account> ToAccounts(this IEnumerable<XeroMessages.Account> xeroAccounts)
        {
            if (xeroAccounts == null)
            {
                throw new ArgumentNullException(nameof(xeroAccounts));
            }

            return xeroAccounts.Select(x => x.ToAccount());
        }

        /// <summary>
        /// Converts Xero account into account.
        /// </summary>
        /// <param name="xeroAccount">Xero account.</param>
        /// <returns>Account.</returns>
        public static Account ToAccount(this XeroMessages.Account xeroAccount)
        {
            if (xeroAccount == null)
            {
                throw new ArgumentNullException(nameof(xeroAccount));
            }

            return new Account
            {
                Name = xeroAccount.Name,
                Description = xeroAccount.Description
            };
        }
    }
}
