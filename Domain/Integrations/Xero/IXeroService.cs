﻿using System;
using System.Threading.Tasks;

namespace AIYP.Integrations.Xero
{
    /// <summary>
    /// Xero integration service interface.
    /// </summary>
    public interface IXeroService
    {
        /// <summary>
        /// Returns Xero data.
        /// </summary>
        /// <param name="startDate">Start date.</param>
        /// <param name="endDate">End date.</param>
        /// <returns>Xero data.</returns>
        Task<XeroData> GetDataAsync(DateTime startDate, DateTime? endDate);
    }
}
