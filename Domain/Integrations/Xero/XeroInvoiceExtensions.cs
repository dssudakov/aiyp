﻿using AIYP.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using XeroMessages = AIYP.Gateways.Xero.Messages;

namespace AIYP.Integrations.Xero
{
    /// <summary>
    /// Xero invoices extensions.
    /// </summary>
    static class XeroInvoiceExtensions
    {
        /// <summary>
        /// Converts Xero invoices into invoices.
        /// </summary>
        /// <param name="xeroInvoices">Xero invoices.</param>
        /// <returns>Invoices.</returns>
        public static IEnumerable<Invoice> ToInvoices(this IEnumerable<XeroMessages.Invoice> xeroInvoices)
        {
            if (xeroInvoices == null)
            {
                throw new ArgumentNullException(nameof(xeroInvoices));
            }

            return xeroInvoices.Select(x => x.ToInvoice());
        }

        /// <summary>
        /// Converts Xero invoice into invoice.
        /// </summary>
        /// <param name="xeroInvoice">Xero invoice.</param>
        /// <returns>Invoice.</returns>
        public static Invoice ToInvoice(this XeroMessages.Invoice xeroInvoice)
        {
            if (xeroInvoice == null)
            {
                throw new ArgumentNullException(nameof(xeroInvoice));
            }

            var invoice = new Invoice
            {
                AmountPaid = xeroInvoice.AmountPaid,
                Date = xeroInvoice.Date,
                DueDate = xeroInvoice.DueDate,
                InvoiceId = xeroInvoice.InvoiceId,
                InvoiceNumber = xeroInvoice.InvoiceNumber,
                PaymentDate = GetPaymentDate(xeroInvoice),
                Status = GetStatus(xeroInvoice),
                Total = xeroInvoice.Total,
                LineItems = xeroInvoice.LineItems.Select(GetLineItem).ToList()
            };

            return invoice;
        }

        private static DateTime GetPaymentDate(XeroMessages.Invoice xeroInvoice)
        {
            return
                xeroInvoice.ExpectedPaymentDate ??
                xeroInvoice.PlannedPaymentDate ??
                DateTime.MinValue;
        }

        private static InvoiceStatus GetStatus(XeroMessages.Invoice xeroInvoice)
        {
            switch(xeroInvoice.Status)
            {
                case XeroMessages.InvoiceStatus.Authorised:
                    return InvoiceStatus.Authorised;
                case XeroMessages.InvoiceStatus.Deleted:
                    return InvoiceStatus.Deleted;
                case XeroMessages.InvoiceStatus.Draft:
                    return InvoiceStatus.Draft;
                case XeroMessages.InvoiceStatus.Paid:
                    return InvoiceStatus.Paid;
                case XeroMessages.InvoiceStatus.Submitted:
                    return InvoiceStatus.Submitted;
                case XeroMessages.InvoiceStatus.Voided:
                    return InvoiceStatus.Voided;
                default:
                    throw new InvalidOperationException("Invalid invoice status.");
            }
        }

        private static LineItem GetLineItem(XeroMessages.InvoiceLineItem xeroInvoiceLineItem)
        {
            return new LineItem
            {
                Description = xeroInvoiceLineItem.Description,
                DiscountRate = xeroInvoiceLineItem.DiscountRate,
                ItemCode = xeroInvoiceLineItem.ItemCode,
                LineAmount = xeroInvoiceLineItem.LineAmount,
                Quantity = xeroInvoiceLineItem.Quantity,
                TaxAmount = xeroInvoiceLineItem.TaxAmount,
                UnitAmount = xeroInvoiceLineItem.UnitAmount,
            };
        }
    }
}
