﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NumberExtensions.cs" company="">
//   
// </copyright>
// <summary>
//   The number extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Extensions
{
    /// <summary>
    ///     The number extensions.
    /// </summary>
    public static class NumberExtensions
    {
        /// <summary>
        /// The divide.
        /// </summary>
        /// <param name="amount">
        /// The amount.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal Divide(this decimal amount, decimal value)
        {
            if (amount == 0 || value == 0)
            {
                return 0;
            }

            return amount / value;
        }

        /// <summary>
        /// The between.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="lower">
        /// The lower.
        /// </param>
        /// <param name="higher">
        /// The higher.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool Between(this decimal value, decimal lower, decimal higher)
        {
            return value >= lower && value <= higher;
        }

        /// <summary>
        /// The between.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="lower">
        /// The lower.
        /// </param>
        /// <param name="higher">
        /// The higher.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool Between(this int value, int lower, int higher)
        {
            return value >= lower && value <= higher;
        }

        /// <summary>
        /// The divide.
        /// </summary>
        /// <param name="amount">
        /// The amount.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal Divide(this int amount, int value)
        {
            if (amount == 0 || value == 0)
            {
                return 0;
            }

            return amount / (decimal)value;
        }

        /// <summary>
        /// The between.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="range">
        /// The range.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool Between(this decimal value, Range range)
        {
            if (range.High == null)
            {
                return value >= range.Low;
            }

            if (range.Low == null)
            {
                return value <= range.High;
            }

            return value >= range.Low && value <= range.High;
        }

        /// <summary>
        /// The between.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="lower">
        /// The lower.
        /// </param>
        /// <param name="higher">
        /// The higher.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool Between(this double value, decimal lower, decimal higher)
        {
            return (decimal)value >= lower && (decimal)value <= higher;
        }

        /// <summary>
        /// The between.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="lower">
        /// The lower.
        /// </param>
        /// <param name="higher">
        /// The higher.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool Between(this decimal value, decimal? lower, decimal? higher)
        {
            if (higher == null)
            {
                return value >= lower;
            }

            if (lower == null)
            {
                return value <= higher;
            }

            return value >= lower && value <= higher;
        }
    }
}