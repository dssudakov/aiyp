﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CollectionExtensions.cs" company="">
//   
// </copyright>
// <summary>
//   The collection extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Extensions
{
    using System.Collections.Generic;

    /// <summary>
    ///     The collection extensions.
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        ///     The lock me.
        /// </summary>
        private static readonly object LockMe = new object();

        /// <summary>
        /// The add and return.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public static List<T> AddAndReturn<T>(this List<T> list, T value)
        {
            if (value == null)
            {
                return list;
            }

            lock (LockMe)
            {
                list.Add(value);
            }

            return list;
        }

        /// <summary>
        /// The add or default.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        public static void AddOrDefault<T>(this List<T> list, T value)
        {
            if (value == null)
            {
                return;
            }

            lock (LockMe)
            {
                list.Add(value);
            }
        }

        /// <summary>
        /// The add range or default.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        public static void AddRangeOrDefault<T>(this List<T> list, IEnumerable<T> value)
        {
            if (value == null)
            {
                return;
            }

            lock (LockMe)
            {
                list.AddRange(value);
            }
        }
    }
}