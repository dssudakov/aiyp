﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DateExtensions.cs" company="">
//   
// </copyright>
// <summary>
//   The data extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Extensions
{
    using System;

    /// <summary>
    ///     The data extensions.
    /// </summary>
    public static class DataExtensions
    {
        /// <summary>
        /// The get start of finanical year.
        /// </summary>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        public static DateTime GetStartOfFinanicalYear(this DateTime endDate)
        {
            return endDate.Month.Between(1, 6) ? new DateTime(endDate.Year - 1, 7, 1) : new DateTime(endDate.Year, 7, 1);
        }

        /// <summary>
        /// The prorata factor to start of financial year.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal ProrataFactorToStartOfFinancialYear(this DateTime? dateTime)
        {
            var enddate = dateTime ?? DateTime.Now;

            var startDate = enddate.GetStartOfFinanicalYear();

            var numberOfDays = enddate.Subtract(startDate).Days;

            return numberOfDays / 365m;
        }

        /// <summary>
        /// The start of week.
        /// </summary>
        /// <param name="dt">
        /// The dt.
        /// </param>
        /// <param name="startOfWeek">
        /// The start of week.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            var diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        /// <summary>
        /// The prorata factor to start of financial year.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal ProrataFactorToStartOfFinancialYear(this DateTime dateTime)
        {
            var startDate = dateTime.GetStartOfFinanicalYear();

            var numberOfDays = dateTime.Subtract(startDate).Days;

            return numberOfDays / 365m;
        }

        /// <summary>
        /// The get gst range.
        /// </summary>
        /// <param name="currentDateTime">
        /// The current date time.
        /// </param>
        /// <param name="gstMethod">
        /// The gst method.
        /// </param>
        /// <returns>
        /// The <see cref="DateRange"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public static DateRange GetGstRange(this DateTime currentDateTime, DateRangeTypes gstMethod)
        {
            var range = new DateRange();

            switch (gstMethod)
            {
                case DateRangeTypes.Yearly:
                    range.StartDate = currentDateTime.GetStartOfFinanicalYear();
                    range.EndDate = range.StartDate.AddYears(1).AddDays(-1);
                    break;
                case DateRangeTypes.Monthly:
                    range.StartDate = new DateTime(currentDateTime.Year, currentDateTime.Month, 1);
                    range.EndDate = new DateTime(currentDateTime.Year, currentDateTime.Month + 1, 1).AddDays(-1);
                    break;
                case DateRangeTypes.Quarterly:
                    if (currentDateTime.Month >= 1 && currentDateTime.Month <= 3)
                    {
                        range.StartDate = new DateTime(currentDateTime.Year, 1, 1);
                        range.EndDate = new DateTime(currentDateTime.Year, 4, 1).AddDays(-1);
                    }
                    else if (currentDateTime.Month >= 4 && currentDateTime.Month <= 6)
                    {
                        range.StartDate = new DateTime(currentDateTime.Year, 4, 1);
                        range.EndDate = new DateTime(currentDateTime.Year, 7, 1).AddDays(-1);
                    }
                    else if (currentDateTime.Month >= 7 && currentDateTime.Month <= 9)
                    {
                        range.StartDate = new DateTime(currentDateTime.Year, 7, 1);
                        range.EndDate = new DateTime(currentDateTime.Year, 10, 1).AddDays(-1);
                    }
                    else
                    {
                        range.StartDate = new DateTime(currentDateTime.Year, 10, 1);
                        range.EndDate = new DateTime(currentDateTime.Year + 1, 1, 1).AddDays(-1);
                    }

                    break;
                default:
                    throw new NotImplementedException();
            }

            return range;
        }
    }
}