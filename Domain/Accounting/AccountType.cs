﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountType.cs" company="">
//   
// </copyright>
// <summary>
//   The account type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Accounting
{
    /// <summary>
    ///     The account type.
    /// </summary>
    public enum AccountType
    {
        /// <summary>
        ///     The assets.
        /// </summary>
        Assets, 

        /// <summary>
        ///     The liabilities.
        /// </summary>
        Liabilities, 

        /// <summary>
        ///     The expenses.
        /// </summary>
        Expenses, 

        /// <summary>
        ///     The equity.
        /// </summary>
        Equity, 

        /// <summary>
        ///     The revenue.
        /// </summary>
        Revenue, 

        /// <summary>
        ///     The current assets.
        /// </summary>
        CurrentAssets, 

        /// <summary>
        ///     The current liabilities.
        /// </summary>
        CurrentLiabilities
    }
}