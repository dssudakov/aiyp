﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FinancialData.cs" company="">
//   
// </copyright>
// <summary>
//   The financial data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Accounting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AIYP.Extensions;

    /// <summary>
    ///     The financial data.
    /// </summary>
    public class FinancialData
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FinancialData" /> class.
        /// </summary>
        public FinancialData()
        {
            this.Date = new DateTime();
            this.Assets = new List<Account>();
            this.Liabilties = new List<Account>();
            this.Equity = new List<Account>();
            this.Revenue = new List<Account>();
            this.Expenses = new List<Account>();
            this.CurrentAssets = new List<Account>();
            this.CurrentLiabilities = new List<Account>();

            this.CostOfSales = new decimal();

            this.SummaryValues = new Dictionary<SummaryType, decimal>();
        }

        /// <summary>
        ///     Gets or sets the date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        ///     Gets or sets the assets.
        /// </summary>
        public List<Account> Assets { get; set; }

        /// <summary>
        ///     Gets or sets the liabilties.
        /// </summary>
        public List<Account> Liabilties { get; set; }

        /// <summary>
        ///     Gets or sets the equity.
        /// </summary>
        public List<Account> Equity { get; set; }

        /// <summary>
        ///     Gets or sets the revenue.
        /// </summary>
        public List<Account> Revenue { get; set; }

        /// <summary>
        ///     Gets or sets the expenses.
        /// </summary>
        public List<Account> Expenses { get; set; }

        /// <summary>
        ///     Gets or sets the current assets.
        /// </summary>
        public List<Account> CurrentAssets { get; set; }

        /// <summary>
        ///     Gets or sets the current liabilities.
        /// </summary>
        public List<Account> CurrentLiabilities { get; set; }

        /// <summary>
        ///     Gets or sets the cost of sales.
        /// </summary>
        public decimal CostOfSales { get; set; }

        /// <summary>
        ///     Gets or sets the summary values.
        /// </summary>
        public Dictionary<SummaryType, decimal> SummaryValues { get; set; }

        /// <summary>
        ///     The is empty.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool IsEmpty()
        {
            var value = this.Assets.Sum(f => f.Value) + this.Liabilties.Sum(f => f.Value)
                        + this.Equity.Sum(f => f.Value) + this.Revenue.Sum(f => f.Value)
                        + this.Expenses.Sum(f => f.Value);
            return value == 0;
        }

        /// <summary>
        ///     The total equity.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal TotalEquity()
        {
            return this.Equity?.Sum(f => f.Value) ?? decimal.Zero;
        }

        /// <summary>
        ///     The total liabilties.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal TotalLiabilties()
        {
            return this.Liabilties?.Sum(f => f.Value) ?? decimal.Zero;
        }

        /// <summary>
        ///     The total assets.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal TotalAssets()
        {
            return this.Assets?.Sum(f => f.Value) ?? decimal.Zero;
        }

        /// <summary>
        ///     The total current assets.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal TotalCurrentAssets()
        {
            return this.CurrentAssets?.Sum(f => f.Value) ?? decimal.Zero;
        }

        /// <summary>
        ///     The total current liabilities.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal TotalCurrentLiabilities()
        {
            return this.CurrentLiabilities?.Sum(f => f.Value) ?? decimal.Zero;
        }

        /// <summary>
        /// The estimate full year.
        /// </summary>
        /// <param name="amount">
        /// The amount.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        private decimal EstimateFullYear(decimal amount)
        {
            const decimal TotalDays = 365m;

            decimal passedDays = this.Date.Subtract(this.Date.GetStartOfFinanicalYear()).Days;

            return amount + (amount * (1 - (passedDays / TotalDays)));
        }

        /// <summary>
        ///     The gst amount.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal GSTAmount()
        {
            return this.Liabilties.Where(liabilties => liabilties.Name.ToLower().Contains("gst")).Sum(f => f.Value);
        }

        #region "P & L"

        /// <summary>
        /// The total income.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalIncome(bool proRata = false)
        {
            var revenue = this.Revenue?.Sum(f => f.Value) ?? decimal.Zero;

            return proRata == false ? revenue : this.EstimateFullYear(revenue);
        }

        /// <summary>
        /// The total expenses.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalExpenses(bool proRata = false)
        {
            var expense = this.Expenses?.Sum(f => f.Value) ?? decimal.Zero;

            return proRata == false ? expense : this.EstimateFullYear(expense);
        }

        /// <summary>
        /// The total annual turn over.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalAnnualTurnOver(bool proRata = false)
        {
            return proRata == false ? this.TotalIncome() : this.EstimateFullYear(this.TotalIncome());
        }

        /// <summary>
        /// The total sales.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalSales(bool proRata = false)
        {
            return proRata == false ? this.TotalIncome() : this.EstimateFullYear(this.TotalIncome());
        }

        /// <summary>
        /// The total rent.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalRent(bool proRata = false)
        {
            var rent =
                this.Expenses.Where(
                    expense => expense.Name.ToLower().Contains("rent") || expense.Description.ToLower().Contains("rent"))
                    .Sum(expense => expense.Value);

            return proRata == false ? rent : this.EstimateFullYear(rent);
        }

        /// <summary>
        /// The total labour.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalLabour(bool proRata = false)
        {
            var labour =
                this.Expenses.Where(
                    expense =>
                    expense.Name.ToLower().Contains("labour") || expense.Description.ToLower().Contains("labour")
                    || expense.Name.ToLower().Contains("subcontractors")
                    || expense.Description.ToLower().Contains("subcontractors")
                    || expense.Name.ToLower().Contains("wages") || expense.Description.ToLower().Contains("wages"))
                    .Sum(expense => expense.Value);

            return proRata == false ? labour : this.EstimateFullYear(labour);
        }

        /// <summary>
        /// The total motor vehicle expense.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalMotorVehicleExpense(bool proRata = false)
        {
            var motor =
                this.Expenses.Where(
                    expense =>
                    expense.Name.ToLower().Contains("motor vehicle")
                    || expense.Description.ToLower().Contains("motor vehicle")).Sum(expense => expense.Value);

            return proRata == false ? motor : this.EstimateFullYear(motor);
        }

        /// <summary>
        /// The total cost of sales.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalCostOfSales(bool proRata = false)
        {
            return proRata == false ? this.CostOfSales : this.EstimateFullYear(this.CostOfSales);
        }

        /// <summary>
        /// The total captial purchases.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalCaptialPurchases(bool proRata = false)
        {
            // TODO : Need to calc
            return proRata == false ? 0m : this.EstimateFullYear(0m);
        }

        /// <summary>
        /// The total non capital purchases.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalNonCapitalPurchases(bool proRata = false)
        {
            // TODO : Need to calc
            return proRata == false ? 0m : this.EstimateFullYear(0m);
        }

        /// <summary>
        /// The total stock.
        /// </summary>
        /// <param name="proRata">
        /// The pro rata.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal TotalStock(bool proRata = false)
        {
            // TODO : Need to calc
            return proRata == false ? 0m : this.EstimateFullYear(0m);
        }

        #endregion

        #region "Ratios"

        /// <summary>
        ///     The debt equity ratio.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal DebtEquityRatio()
        {
            return this.TotalLiabilties() / this.TotalEquity();
        }

        /// <summary>
        ///     The gross profit margin.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal GrossProfitMargin()
        {
            return this.SummaryValues.Where(f => f.Key == SummaryType.GrossProfitMargin).Select(f => f.Value).Last();
        }

        /// <summary>
        ///     The net profit margin.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal NetProfitMargin()
        {
            return this.SummaryValues.Where(f => f.Key == SummaryType.NetProfitMargin).Select(f => f.Value).Last();
        }

        /// <summary>
        ///     The roi.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal ROI()
        {
            return this.SummaryValues.Where(f => f.Key == SummaryType.ROI).Select(f => f.Value).Last();
        }

        /// <summary>
        ///     The average day debtor.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal AverageDayDebtor()
        {
            return this.SummaryValues.Where(f => f.Key == SummaryType.AverageDayDebtor).Select(f => f.Value).Last();
        }

        /// <summary>
        ///     The average day creditor.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal AverageDayCreditor()
        {
            return this.SummaryValues.Where(f => f.Key == SummaryType.AverageDayCreditor).Select(f => f.Value).Last();
        }

        /// <summary>
        ///     The short term cash forecast.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal ShortTermCashForecast()
        {
            return this.SummaryValues.Where(f => f.Key == SummaryType.ShortTermCashForecast).Select(f => f.Value).Last();
        }

        /// <summary>
        ///     The total assets to total liabilities.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal TotalAssetsToTotalLiabilities()
        {
            return
                this.SummaryValues.Where(f => f.Key == SummaryType.TotalAssetsToLiability).Select(f => f.Value).Last();
        }

        /// <summary>
        ///     The total current asset to total current liabilities.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal TotalCurrentAssetToTotalCurrentLiabilities()
        {
            return
                this.SummaryValues.Where(f => f.Key == SummaryType.CurrentAssetToCurrentLiability)
                    .Select(f => f.Value)
                    .Last();
        }

        /// <summary>
        ///     The quick ratio.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal QuickRatio()
        {
            return (this.TotalCurrentAssets() - this.TotalStock()) / this.TotalCurrentLiabilities();
        }

        /// <summary>
        ///     The roe.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal ROE()
        {
            return this.TotalIncome() / this.TotalEquity();
        }

        /// <summary>
        ///     The net working ratio.
        /// </summary>
        /// <returns>
        ///     The <see cref="decimal" />.
        /// </returns>
        public decimal NetWorkingRatio()
        {
            return this.TotalCurrentAssetToTotalCurrentLiabilities();
        }

        #endregion
    }
}