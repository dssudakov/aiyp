﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Contacts.cs" company="">
//   
// </copyright>
// <summary>
//   The contact.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Accounting
{
    /// <summary>
    ///     The contact.
    /// </summary>
    public class Contact
    {
        /// <summary>
        ///     Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets the abn.
        /// </summary>
        public string ABN { get; set; }

        /// <summary>
        ///     Gets or sets the acn.
        /// </summary>
        public string ACN { get; set; }

        /// <summary>
        ///     Gets or sets the company name.
        /// </summary>
        public string CompanyName { get; set; }
    }
}