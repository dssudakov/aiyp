﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Account.cs" company="">
//   
// </copyright>
// <summary>
//   The account.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Accounting
{
    /// <summary>
    ///     The account.
    /// </summary>
    public class Account
    {
        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        ///     Gets or sets the provider id.
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        ///     Gets or sets the description.
        /// </summary>
        public string Description { get; set; }
    }
}