﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LineItem.cs" company="">
//   
// </copyright>
// <summary>
//   The line item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Accounting
{
    /// <summary>
    ///     The line item.
    /// </summary>
    public class LineItem
    {
        /// <summary>
        ///     The description.
        /// </summary>
        public string Description;

        /// <summary>
        ///     The discount rate.
        /// </summary>
        public decimal? DiscountRate;

        /// <summary>
        ///     The item code.
        /// </summary>
        public string ItemCode;

        /// <summary>
        ///     The line amount.
        /// </summary>
        public decimal? LineAmount;

        /// <summary>
        ///     The quantity.
        /// </summary>
        public decimal? Quantity;

        /// <summary>
        ///     The tax amount.
        /// </summary>
        public decimal? TaxAmount;

        /// <summary>
        ///     The unit amount.
        /// </summary>
        public decimal? UnitAmount;
    }
}