// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InvoiceCollectionExtension.cs" company="">
//   
// </copyright>
// <summary>
//   The invoice collection extension.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Accounting.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///     The invoice collection extension.
    /// </summary>
    public static class InvoiceCollectionExtension
    {
        /// <summary>
        /// The get first invoice date.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        public static DateTime GetFirstInvoiceDate(this Dictionary<string, List<Invoice>> debtorList)
        {
            var firstDate = DateTime.Now;

            foreach (var item in debtorList)
            {
                foreach (var invoice in item.Value)
                {
                    if (invoice.Date < firstDate)
                    {
                        firstDate = invoice.Date;
                    }
                }
            }

            return firstDate;
        }

        /// <summary>
        /// The get amount per week.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public static Dictionary<DateTime, decimal> GetAmountPerWeek(
            this Dictionary<string, List<Invoice>> debtorList, 
            DateTime startDate, 
            DateTime endDate)
        {
            var result = new Dictionary<DateTime, decimal>();

            // get first  start date
            var firstInvoiceDate = debtorList.GetFirstInvoiceDate();

            if (endDate < firstInvoiceDate)
            {
                endDate = firstInvoiceDate;
            }

            for (var i = startDate; i > endDate; i = i.AddDays(-7))
            {
                // get all invoices from the 
                var rangeEndtDate = i;
                var rangeStartDate = i.AddDays(-7);

                result.Add(rangeStartDate, debtorList.GetTotalInvoices(rangeStartDate, rangeEndtDate));
            }

            return result;
        }

        /// <summary>
        /// The get amount per month.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public static Dictionary<DateTime, decimal> GetAmountPerMonth(
            this Dictionary<string, List<Invoice>> debtorList, 
            DateTime startDate, 
            DateTime endDate)
        {
            var result = new Dictionary<DateTime, decimal>();

            // get first  start date
            var firstInvoiceDate = debtorList.GetFirstInvoiceDate();

            if (endDate < firstInvoiceDate)
            {
                endDate = firstInvoiceDate;
            }

            for (var i = startDate; i > endDate; i = i.AddMonths(-1))
            {
                // get all invoices from the 
                var rangeEndtDate = i;
                var rangeStartDate = i.AddMonths(-1);

                result.Add(rangeStartDate, debtorList.GetTotalInvoices(rangeStartDate, rangeEndtDate));
            }

            return result;
        }

        /// <summary>
        /// The get average per week.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal GetAveragePerWeek(
            this Dictionary<string, List<Invoice>> debtorList, 
            DateTime startDate, 
            DateTime endDate)
        {
            decimal result = 0;
            var count = 0;

            // get first  start date
            var firstInvoiceDate = debtorList.GetFirstInvoiceDate();

            if (endDate < firstInvoiceDate)
            {
                endDate = firstInvoiceDate;
            }

            for (var i = startDate; i > endDate; i = i.AddDays(-7))
            {
                // get all invoices from the 
                var rangeEndtDate = i;
                var rangeStartDate = i.AddDays(-7);

                result += debtorList.GetTotalInvoices(rangeStartDate, rangeEndtDate);
                count++;
            }

            if (result == 0 || count == 0)
            {
                return 0;
            }

            return result / count;
        }

        /// <summary>
        /// The get average per month.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal GetAveragePerMonth(
            this Dictionary<string, List<Invoice>> debtorList, 
            DateTime startDate, 
            DateTime endDate)
        {
            decimal result = 0;
            var count = 0;

            // get first  start date
            var firstInvoiceDate = debtorList.GetFirstInvoiceDate();

            if (endDate < firstInvoiceDate)
            {
                endDate = firstInvoiceDate;
            }

            for (var i = startDate; i > endDate; i = i.AddMonths(-1))
            {
                // get all invoices from the 
                var rangeEndtDate = i;
                var rangeStartDate = i.AddMonths(-1);

                result += debtorList.GetTotalInvoices(rangeStartDate, rangeEndtDate);
                count++;
            }

            if (result == 0 || count == 0)
            {
                return 0;
            }

            return result / count;
        }

        /// <summary>
        /// The get days to pay.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public static Dictionary<DateTime, int> GetDaysToPay(this List<Invoice> list)
        {
            var result = new Dictionary<DateTime, int>();

            foreach (var invoice in list)
            {
                if (invoice.AmountDue == 0 && invoice.Total > 0)
                {
                    var day = invoice.PaymentDate.Subtract(invoice.Date).Days;

                    if (!result.ContainsKey(invoice.Date))
                    {
                        result.Add(invoice.Date, day);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// The get over due invoices.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public static List<Invoice> GetOverDueInvoices(this List<Invoice> list)
        {
            return
                list.Where(invoice => (invoice.AmountPaid < invoice.Total) && invoice.DueDate.Date < DateTime.Now.Date)
                    .ToList();
        }

        /// <summary>
        /// The get due invoices.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public static List<Invoice> GetDueInvoices(this List<Invoice> list)
        {
            return list.Where(invoice => (invoice.AmountPaid < invoice.Total)).ToList();
        }

        /// <summary>
        /// The get average payment overdue in days.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The <see cref="double"/>.
        /// </returns>
        public static double GetAveragePaymentOverdueInDays(this List<Invoice> list)
        {
            var result = 0;
            var count = 0;

            foreach (var invoice in list.Where(invoice => invoice.AmountDue == 0))
            {
                if (invoice.PaymentDate != DateTime.MinValue)
                {
                    result += invoice.PaymentDate.Subtract(invoice.DueDate).Days;
                    count++;
                }
            }

            if (result == 0 || count == 0)
            {
                return 0;
            }

            if (result < 0)
            {
                var value = Math.Abs(result) / count;

                return value * -1;
            }

            return result / count;

            // return
            // list.Where(invoice => invoice.AmountDue == 0)
            // .Average(invoice => invoice.PaymentDate.Subtract(invoice.DueDate).Days);
        }

        /// <summary>
        /// The get average payment in days.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The <see cref="double"/>.
        /// </returns>
        public static double GetAveragePaymentInDays(this List<Invoice> list)
        {
            var result = 0;
            var count = 0;

            foreach (var invoice in list.Where(invoice => invoice.AmountDue == 0))
            {
                if (invoice.PaymentDate != DateTime.MinValue)
                {
                    result += invoice.PaymentDate.Subtract(invoice.Date).Days;
                    count++;
                }
            }

            if (result == 0 || count == 0)
            {
                return 0;
            }

            if (result < 0)
            {
                var value = Math.Abs(result) / count;

                return value * -1;
            }

            return result / count;

            // return
            // list.Where(invoice => invoice.AmountDue == 0)
            // .Average(invoice => invoice.PaymentDate.Subtract(invoice.DueDate).Days);
        }

        /// <summary>
        /// The get total invoice after date.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <param name="afterDate">
        /// The after date.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal GetTotalInvoiceAfterDate(this List<Invoice> list, DateTime afterDate)
        {
            var result = list.Where(f => f.Date > afterDate);

            return result.Any() ? result.Average(f => f.Total) : 0;
        }

        /// <summary>
        /// The total overdue.
        /// </summary>
        /// <param name="invoices">
        /// The invoices.
        /// </param>
        /// <param name="date">
        /// The date.
        /// </param>
        /// <param name="daysOverdue">
        /// The days overdue.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal TotalOverdue(this List<Invoice> invoices, DateTime date, int daysOverdue)
        {
            return TotalOverdue(invoices, date.AddDays(-daysOverdue));
        }

        /// <summary>
        /// The total.
        /// </summary>
        /// <param name="invoices">
        /// The invoices.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal Total(this List<Invoice> invoices)
        {
            return invoices.Sum(f => f.Total);
        }

        /// <summary>
        /// The total.
        /// </summary>
        /// <param name="invoices">
        /// The invoices.
        /// </param>
        /// <param name="startDateTime">
        /// The start date time.
        /// </param>
        /// <param name="endDateTime">
        /// The end date time.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal Total(this List<Invoice> invoices, DateTime startDateTime, DateTime endDateTime)
        {
            return invoices.Where(f => f.Date >= startDateTime && f.Date <= endDateTime).Sum(f => f.Total);
        }

        /// <summary>
        /// The total due.
        /// </summary>
        /// <param name="invoices">
        /// The invoices.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal TotalDue(this List<Invoice> invoices)
        {
            return invoices.Where(invoice => invoice.AmountDue > 0).Sum(invoice => invoice.AmountDue);
        }

        /// <summary>
        /// The total overdue.
        /// </summary>
        /// <param name="invoices">
        /// The invoices.
        /// </param>
        /// <param name="date">
        /// The date.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal TotalOverdue(this List<Invoice> invoices, DateTime date)
        {
            return
                invoices.Where(invoice => invoice.AmountDue > 0 && invoice.DueDate < date)
                    .Sum(invoice => invoice.AmountDue);
        }

        /// <summary>
        /// The total overdue.
        /// </summary>
        /// <param name="invoices">
        /// The invoices.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal TotalOverdue(this List<Invoice> invoices)
        {
            return TotalOverdue(invoices, DateTime.Now);
        }

        /// <summary>
        /// The total overdue.
        /// </summary>
        /// <param name="invoices">
        /// The invoices.
        /// </param>
        /// <param name="daysOverdue">
        /// The days overdue.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal TotalOverdue(this List<Invoice> invoices, int daysOverdue)
        {
            return TotalOverdue(invoices, DateTime.Now.AddDays(-daysOverdue));
        }

        /// <summary>
        /// The remove draft invoice.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public static Dictionary<string, List<Invoice>> RemoveDraftInvoice(
            this Dictionary<string, List<Invoice>> debtorList)
        {
            var list = new Dictionary<string, List<Invoice>>();

            foreach (var key in debtorList.Keys)
            {
                var invoiceList = new List<Invoice>();

                foreach (var invoice in debtorList[key])
                {
                    if (invoice.Status != InvoiceStatus.Draft)
                    {
                        invoiceList.Add(invoice);
                    }
                }

                list.Add(key, invoiceList);
            }

            return list;
        }

        /// <summary>
        /// The get last months.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <param name="months">
        /// The months.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public static Dictionary<string, List<Invoice>> GetLastMonths(
            this Dictionary<string, List<Invoice>> debtorList, 
            int months)
        {
            var list = new Dictionary<string, List<Invoice>>();

            foreach (var key in debtorList.Keys)
            {
                var invoiceList = new List<Invoice>();

                foreach (var invoice in debtorList[key])
                {
                    if (invoice.Date > DateTime.Now.AddMonths(-months))
                    {
                        invoiceList.Add(invoice);
                    }
                }

                list.Add(key, invoiceList);
            }

            return list;
        }

        /// <summary>
        /// The get total hours worked.
        /// </summary>
        /// <param name="invoices">
        /// The invoices.
        /// </param>
        /// <param name="itemCodes">
        /// The item codes.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal GetTotalHoursWorked(this List<Invoice> invoices, List<string> itemCodes)
        {
            var result = new decimal();

            foreach (var invoice in invoices)
            {
                result += invoice.GetTotalHoursWorked(itemCodes);
            }

            return result;
        }

        /// <summary>
        /// The get average hourly rate.
        /// </summary>
        /// <param name="invoices">
        /// The invoices.
        /// </param>
        /// <param name="itemCodes">
        /// The item codes.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal GetAverageHourlyRate(this List<Invoice> invoices, List<string> itemCodes)
        {
            var hours = new decimal();
            var total = new decimal();

            foreach (var invoice in invoices)
            {
                hours += invoice.GetTotalHoursWorked(itemCodes);
                total += invoice.GetTotalAmountForHoursWorked(itemCodes);
            }

            if (hours == 0 || total == 0)
            {
                return 0;
            }

            return total / hours;
        }
    }
}