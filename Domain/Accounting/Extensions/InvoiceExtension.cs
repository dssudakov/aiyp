﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InvoiceExtension.cs" company="">
//   
// </copyright>
// <summary>
//   The invoice extension.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Accounting.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///     The invoice extension.
    /// </summary>
    public static class InvoiceExtension
    {
        /// <summary>
        /// The get total invoices.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <param name="rangeStartDate">
        /// The range start date.
        /// </param>
        /// <param name="rangeEndDate">
        /// The range end date.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal GetTotalInvoices(
            this Dictionary<string, List<Invoice>> debtorList, 
            DateTime rangeStartDate, 
            DateTime rangeEndDate)
        {
            return
                debtorList.Sum(
                    debtor =>
                    debtor.Value.Where(invoice => invoice.Date >= rangeStartDate && invoice.Date <= rangeEndDate)
                        .Sum(invoice => invoice.Total));
        }

        /// <summary>
        /// The get average per range.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <param name="dateRange">
        /// The date range.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public static decimal GetAveragePerRange(
            this Dictionary<string, List<Invoice>> debtorList, 
            DateRange dateRange, 
            DateTime startDate)
        {
            decimal totalInvoices = 0;

            var currentStartDate = dateRange.GetPreviousDateRange(1).StartDate;

            var counter = 1;

            for (; startDate < currentStartDate; counter++)
            {
                var currentRange = dateRange.GetPreviousDateRange(counter);
                var rangeEndtDate = currentRange.EndDate;
                var rangeStartDate = currentRange.StartDate;

                totalInvoices += debtorList.GetTotalInvoices(rangeStartDate, rangeEndtDate);

                currentStartDate = currentRange.StartDate;
            }

            return totalInvoices / counter;
        }

        /// <summary>
        /// The get amount per week.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public static Dictionary<DateTime, decimal> GetAmountPerWeek(this Dictionary<string, List<Invoice>> debtorList)
        {
            var result = new Dictionary<DateTime, decimal>();

            var startDate = DateTime.Now.AddDays(-7); // this will start the average calc from the prevous week

            for (var i = startDate; i > startDate.AddYears(-1); i = i.AddDays(-7))
            {
                // get all invoices from the 
                var rangeEndtDate = i;
                var rangeStartDate = i.AddDays(-7);

                result.Add(rangeStartDate, debtorList.GetTotalInvoices(rangeStartDate, rangeEndtDate));
            }

            return result;
        }

        /// <summary>
        /// The sum and order by.
        /// </summary>
        /// <param name="debtorList">
        /// The debtor list.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public static Dictionary<string, decimal> SumAndOrderBy(
            this Dictionary<string, List<Invoice>> debtorList, 
            DateTime startDate, 
            DateTime endDate)
        {
            var list = debtorList.ToDictionary(debtor => debtor.Key, debtor => debtor.Value.Total(startDate, endDate));

            return list.OrderByDescending(f => f.Value).ToDictionary(f => f.Key, f => f.Value);
        }
    }
}