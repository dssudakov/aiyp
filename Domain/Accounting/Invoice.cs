﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Invoice.cs" company="">
//   
// </copyright>
// <summary>
//   The invoice.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Accounting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///     The invoice.
    /// </summary>
    public class Invoice
    {
        /// <summary>
        ///     Gets or sets the invoice id.
        /// </summary>
        public string InvoiceId { get; set; }

        /// <summary>
        ///     Gets or sets the date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        ///     Gets or sets the due date.
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        ///     Gets or sets the status.
        /// </summary>
        public InvoiceStatus Status { get; set; }

        /// <summary>
        ///     Gets or sets the total.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        ///     The amount due.
        /// </summary>
        public decimal AmountDue => this.Total - this.AmountPaid;

        /// <summary>
        ///     Gets or sets the amount paid.
        /// </summary>
        public decimal AmountPaid { get; set; }

        /// <summary>
        ///     Gets or sets the payment date.
        /// </summary>
        public DateTime PaymentDate { get; set; }

        /// <summary>
        ///     Gets or sets the invoice number.
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        ///     Gets or sets the line items.
        /// </summary>
        public List<LineItem> LineItems { get; set; }

        /// <summary>
        /// The get total hours worked.
        /// </summary>
        /// <param name="itemCodes">
        /// The item codes.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal GetTotalHoursWorked(List<string> itemCodes)
        {
            return
                this.LineItems.Where(item => itemCodes.Contains(item.ItemCode))
                    .Sum(item => item.Quantity.GetValueOrDefault());
        }

        /// <summary>
        /// The get total amount for hours worked.
        /// </summary>
        /// <param name="itemCodes">
        /// The item codes.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        internal decimal GetTotalAmountForHoursWorked(List<string> itemCodes)
        {
            return
                this.LineItems.Where(item => itemCodes.Contains(item.ItemCode))
                    .Sum(item => item.LineAmount.GetValueOrDefault());
        }
    }
}