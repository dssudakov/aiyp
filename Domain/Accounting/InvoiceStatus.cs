﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InvoiceStatus.cs" company="">
//   
// </copyright>
// <summary>
//   The invoice status.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP.Accounting
{
    /// <summary>
    ///     The invoice status.
    /// </summary>
    public enum InvoiceStatus
    {
        /// <summary>
        ///     The draft.
        /// </summary>
        Draft = 0, 

        /// <summary>
        ///     The submitted.
        /// </summary>
        Submitted = 1, 

        /// <summary>
        ///     The deleted.
        /// </summary>
        Deleted = 2, 

        /// <summary>
        ///     The authorised.
        /// </summary>
        Authorised = 3, 

        /// <summary>
        ///     The paid.
        /// </summary>
        Paid = 4, 

        /// <summary>
        ///     The voided.
        /// </summary>
        Voided = 5
    }
}