﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DateRange.cs" company="">
//   
// </copyright>
// <summary>
//   The date range types.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AIYP
{
    using System;

    using AIYP.Extensions;

    /// <summary>
    ///     The date range types.
    /// </summary>
    public enum DateRangeTypes
    {
        /// <summary>
        ///     The daily.
        /// </summary>
        Daily, 

        /// <summary>
        ///     The weekly.
        /// </summary>
        Weekly, 

        /// <summary>
        ///     The fortnightly.
        /// </summary>
        Fortnightly, 

        /// <summary>
        ///     The monthly.
        /// </summary>
        Monthly, 

        /// <summary>
        ///     The yearly.
        /// </summary>
        Yearly, 

        /// <summary>
        ///     The quarterly.
        /// </summary>
        Quarterly
    }

    /// <summary>
    ///     The date range.
    /// </summary>
    public class DateRange
    {
        /// <summary>
        ///     Gets or sets the start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        ///     Gets or sets the end date.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        ///     Gets or sets the date range types.
        /// </summary>
        public DateRangeTypes DateRangeTypes { get; set; }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public override string ToString()
        {
            if (this.EndDate.Subtract(this.StartDate).Days == 7)
            {
                return string.Format(
                    "From the {0} to {1}", 
                    this.StartDate.ToString("dd MMM yyyy"), 
                    this.EndDate.ToString("dd MMM yyyy"));
            }

            if (this.EndDate.Subtract(this.StartDate).Days >= 28 && this.EndDate.Subtract(this.StartDate).Days <= 31)
            {
                return string.Format(
                    "From the {0} to {1}", 
                    this.StartDate.ToString("dd MMM yyyy"), 
                    this.EndDate.ToString("dd MMM yyyy"));
            }

            return string.Format(
                "From {0} to {1}", 
                this.StartDate.ToString("dd MMM yyyy"), 
                this.EndDate.ToString("dd MMM yyyy"));
        }

        /// <summary>
        /// The get date range.
        /// </summary>
        /// <param name="range">
        /// The range.
        /// </param>
        /// <returns>
        /// The <see cref="DateRange"/>.
        /// </returns>
        public static DateRange GetDateRange(string range)
        {
            switch (range.ToLower())
            {
                case "week":
                    return new DateRange
                               {
                                   StartDate = DateTime.Now.AddDays(-7), 
                                   EndDate = DateTime.Now, 
                                   DateRangeTypes = DateRangeTypes.Weekly
                               };
                case "month":
                    return new DateRange
                               {
                                   StartDate = DateTime.Now.AddMonths(-1), 
                                   EndDate = DateTime.Now, 
                                   DateRangeTypes = DateRangeTypes.Monthly
                               };
                case "fortnight":
                    return new DateRange
                               {
                                   StartDate = DateTime.Now.AddDays(-14), 
                                   EndDate = DateTime.Now, 
                                   DateRangeTypes = DateRangeTypes.Fortnightly
                               };
                case "year":
                    return new DateRange
                               {
                                   StartDate = DateTime.Now.AddYears(-1), 
                                   EndDate = DateTime.Now, 
                                   DateRangeTypes = DateRangeTypes.Yearly
                               };
                case "financialyear":
                    return new DateRange
                               {
                                   StartDate = DateTime.Now.GetStartOfFinanicalYear(), 
                                   EndDate = DateTime.Now, 
                                   DateRangeTypes = DateRangeTypes.Yearly
                               };
                default:
                    return new DateRange
                               {
                                   StartDate = DateTime.Now.AddMonths(-1), 
                                   EndDate = DateTime.Now, 
                                   DateRangeTypes = DateRangeTypes.Monthly
                               };
            }
        }

        /// <summary>
        /// The get previous date range.
        /// </summary>
        /// <param name="previousPeriods">
        /// The previous periods.
        /// </param>
        /// <returns>
        /// The <see cref="DateRange"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public DateRange GetPreviousDateRange(int previousPeriods)
        {
            if (previousPeriods == 0)
            {
                return this;
            }

            var newDateRange = new DateRange { DateRangeTypes = this.DateRangeTypes };

            if (DateRangeTypes.Monthly == this.DateRangeTypes)
            {
                newDateRange.EndDate = this.EndDate.AddMonths(-previousPeriods);
                newDateRange.StartDate = this.StartDate.AddMonths(-previousPeriods);
                return newDateRange;
            }

            if (DateRangeTypes.Daily == this.DateRangeTypes)
            {
                newDateRange.EndDate = this.EndDate.AddDays(-previousPeriods);
                newDateRange.StartDate = this.StartDate.AddDays(-previousPeriods);
                return newDateRange;
            }

            if (DateRangeTypes.Weekly == this.DateRangeTypes)
            {
                var days = previousPeriods * 7;

                newDateRange.EndDate = this.EndDate.AddDays(-days);
                newDateRange.StartDate = this.StartDate.AddDays(-days);
                return newDateRange;
            }

            if (DateRangeTypes.Fortnightly == this.DateRangeTypes)
            {
                var days = previousPeriods * 14;

                newDateRange.EndDate = this.EndDate.AddDays(-days);
                newDateRange.StartDate = this.StartDate.AddDays(-days);
                return newDateRange;
            }

            if (DateRangeTypes.Yearly == this.DateRangeTypes)
            {
                newDateRange.EndDate = this.EndDate.AddYears(-previousPeriods);
                newDateRange.StartDate = this.StartDate.AddYears(-previousPeriods);
                return newDateRange;
            }

            throw new Exception("No Date Range Type selected");
        }

        /// <summary>
        ///     The get next date range.
        /// </summary>
        /// <returns>
        ///     The <see cref="DateRange" />.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public DateRange GetNextDateRange()
        {
            var newDateRange = new DateRange { DateRangeTypes = this.DateRangeTypes };

            if (DateRangeTypes.Monthly == this.DateRangeTypes)
            {
                newDateRange.EndDate = this.EndDate.AddMonths(1);
                newDateRange.StartDate = this.StartDate.AddMonths(1);
                return newDateRange;
            }

            if (DateRangeTypes.Daily == this.DateRangeTypes)
            {
                newDateRange.EndDate = this.EndDate.AddDays(1);
                newDateRange.StartDate = this.StartDate.AddDays(1);
                return newDateRange;
            }

            if (DateRangeTypes.Weekly == this.DateRangeTypes)
            {
                newDateRange.EndDate = this.EndDate.AddDays(7);
                newDateRange.StartDate = this.StartDate.AddDays(7);
                return newDateRange;
            }

            if (DateRangeTypes.Fortnightly == this.DateRangeTypes)
            {
                newDateRange.EndDate = this.EndDate.AddDays(14);
                newDateRange.StartDate = this.StartDate.AddDays(14);
                return newDateRange;
            }

            if (DateRangeTypes.Yearly == this.DateRangeTypes)
            {
                newDateRange.EndDate = this.EndDate.AddYears(1);
                newDateRange.StartDate = this.StartDate.AddYears(1);
                return newDateRange;
            }

            throw new Exception("No Date Range Type selected");
        }

        /// <summary>
        /// The get display value.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public string GetDisplayValue(DateTime dateTime)
        {
            if (this.DateRangeTypes == DateRangeTypes.Daily)
            {
                return dateTime.ToString("d");
            }

            if (this.DateRangeTypes == DateRangeTypes.Monthly)
            {
                return dateTime.ToString("MMM yyyy");
            }

            if (this.DateRangeTypes == DateRangeTypes.Yearly)
            {
                return dateTime.ToString("yyyy");
            }

            if (this.DateRangeTypes == DateRangeTypes.Weekly)
            {
                return dateTime.ToString("d");
            }

            throw new NotImplementedException();
        }

        /// <summary>
        ///     The total days.
        /// </summary>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public int TotalDays()
        {
            return this.EndDate.Subtract(this.StartDate).Days;
        }
    }
}