﻿using System.Threading.Tasks;

namespace AIYP.Gateways.Common
{
    /// <summary>
    /// Gateway HTTP service interface.
    /// </summary>
    public interface IGatewayHttpService
    {
        /// <summary>
        /// Returns response text.
        /// </summary>
        /// <returns>Response text.</returns>
        Task<string> GetAsync();
    }
}
