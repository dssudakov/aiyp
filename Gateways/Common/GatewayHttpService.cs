﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace AIYP.Gateways.Common
{
    /// <summary>
    /// Gateway HTTP service.
    /// </summary>
    public class GatewayHttpService : IGatewayHttpService
    {
        private const string METHOD_GET = "GET";

        private readonly string uri;
        private readonly IDictionary<string, string> headers;
        private readonly string customContentType;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="uri">URI.</param>
        /// <param name="headers">Headers.</param>
        /// <param name="customContentType">Custom content type.</param>
        public GatewayHttpService(
            string uri,
            IDictionary<string, string> headers = null,
            string customContentType = null)
        {
            this.uri = uri;
            this.headers = headers;
            this.customContentType = customContentType;
        }

        /// <summary>
        /// Returns response text.
        /// </summary>
        /// <returns>Response text.</returns>
        public Task<string> GetAsync()
        {
            CheckUri();
            return ExecuteRequestAsync(METHOD_GET, string.Empty);
        }

        private async Task<string> ExecuteRequestAsync(string method, string requestText)
        {
            var request = (HttpWebRequest)WebRequest.Create(this.uri);
            request.Method = method;
            AddCustomContentType(request);
            AddHeaders(request);

            if (SupportsRequestBody(method))
            {
                using (var streamWriter = new StreamWriter(await request.GetRequestStreamAsync()))
                {
                    await streamWriter.WriteAsync(requestText);
                }
            }

            using (var response = (HttpWebResponse)await request.GetResponseAsync())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    return await reader.ReadToEndAsync();
                }
            }
        }

        private void AddHeaders(HttpWebRequest request)
        {
            if (this.headers == null)
            {
                return;
            }

            foreach (var header in this.headers)
            {
                request.Headers.Add(header.Key, header.Value);
            }
        }

        private void AddCustomContentType(HttpWebRequest request)
        {
            if (!string.IsNullOrEmpty(this.customContentType))
            {
                request.ContentType = this.customContentType;
            }
        }

        private void CheckUri()
        {
            if (string.IsNullOrEmpty(this.uri))
                throw new InvalidOperationException();
        }

        private bool SupportsRequestBody(string method)
        {
            return string.Compare(method, METHOD_GET) != 0;
        }
    }
}
