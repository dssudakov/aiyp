﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Contact status codes.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ContactStatus
    {
        /// <summary>
        /// The Contact is active and can be used in transactions.
        /// </summary>
        [EnumMember(Value = "ACTIVE")]
        Active,

        /// <summary>
        /// The Contact is archived and can no longer be used in transactions.
        /// </summary>
        [EnumMember(Value = "ARCHIVED")]
        Archived,

        /// <summary>
        /// The Contact is the subject of a GDPR erasure request and can no longer be used in tranasctions.
        /// </summary>
        [EnumMember(Value = "EXPENSE")]
        GdprRequest
    }
}
