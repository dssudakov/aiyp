﻿using System;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Get data base request.
    /// </summary>
    public abstract class GetRequestBase
    {
        /// <summary>
        /// Start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// End date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Returns where clause of the current request.
        /// </summary>
        public string Where
        {
            get
            {
                var where = string.Format("UpdatedDateUTC>={0}", GetDateParameter(StartDate));
                if (EndDate.HasValue)
                {
                    where += string.Format("&&UpdatedDateUTC<{0}", GetDateParameter(EndDate.Value));
                }

                return where;
            }
        }

        /// <summary>
        /// Returns query of the current request.
        /// </summary>
        public string Query
        {
            get
            {
                return string.IsNullOrEmpty(Where) ? null : string.Format("where={0}", Uri.EscapeDataString(Where));
            }
        }

        /// <summary>
        /// Endpoint.
        /// </summary>
        public abstract string Endpoint { get; }

        private string GetDateParameter(DateTime dateTime)
        {
            return string.Format("DateTime({0},{1},{2})", dateTime.Year, dateTime.Month, dateTime.Day);
        }
    }
}
