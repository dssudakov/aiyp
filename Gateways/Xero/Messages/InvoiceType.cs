﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Invoice type.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum InvoiceType
    {
        /// <summary>
        /// A bill - commonly known as a Accounts Payable or supplier invoice.
        /// </summary>
        [EnumMember(Value = "ACCPAY")]
        AccPay,

        /// <summary>
        /// A sales invoice - commonly known as an Accounts Receivable or customer invoice.
        /// </summary>
        [EnumMember(Value = "ACCREC")]
        AccRec
    }
}
