﻿namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Get accounts request.
    /// </summary>
    public class GetAccountsRequest : GetRequestBase
    {
        /// <summary>
        /// Endpoint.
        /// </summary>
        public override string Endpoint => "Accounts";
    }
}
