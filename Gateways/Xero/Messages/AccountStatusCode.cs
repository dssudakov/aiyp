﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Account status code.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AccountStatusCode
    {
        /// <summary>
        /// Active.
        /// </summary>
        [EnumMember(Value = "ACTIVE")]
        Active,

        /// <summary>
        /// Archived.
        /// </summary>
        [EnumMember(Value = "ARCHIVED")]
        Archived
    }
}
