﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Invoice status.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum InvoiceStatus
    {
        /// <summary>
        /// The default status if this element is not provided with your API call.
        /// Can contain incomplete line items (e.g. missing account codes).
        /// No journals are created so the details will not be used in any reports.
        /// </summary>
        [EnumMember(Value = "DRAFT")]
        Draft,

        /// <summary>
        /// Useful if there is an approval process required.
        /// No journals are created so the details will not be used in any reports.
        /// </summary>
        [EnumMember(Value = "SUBMITTED")]
        Submitted,

        /// <summary>
        /// The "approved" state of an invoice ready for sending to a customer.
        /// Journals are created in Xero so all details will be used in reports.
        /// You can now apply payments to the invoice.
        /// Once an invoice is fully paid invoice the status will change to PAID.
        /// </summary>
        [EnumMember(Value = "AUTHORISED")]
        Authorised,

        /// <summary>
        /// The invoice is fully paid.
        /// </summary>
        [EnumMember(Value = "PAID")]
        Paid,

        /// <summary>
        /// The invoice is deleted.
        /// </summary>
        [EnumMember(Value = "DELETED")]
        Deleted,

        /// <summary>
        /// The invoice is voided.
        /// </summary>
        [EnumMember(Value = "VOIDED")]
        Voided
    }
}
