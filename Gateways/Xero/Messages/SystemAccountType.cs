﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// System account type.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SystemAccountType
    {
        /// <summary>
        /// Accounts Receivable.
        /// </summary>
        [EnumMember(Value = "DEBTORS")]
        Debtors,

        /// <summary>
        /// Accounts Payable.
        /// </summary>
        [EnumMember(Value = "CREDITORS")]
        Creditors,

        /// <summary>
        /// Bank Revaluations.
        /// </summary>
        [EnumMember(Value = "BANKCURRENCYGAIN")]
        BankCurrencyGain,

        /// <summary>
        /// GST / VAT.
        /// </summary>
        [EnumMember(Value = "GST")]
        Gst,

        /// <summary>
        /// GST On Imports.
        /// </summary>
        [EnumMember(Value = "GSTONIMPORTS")]
        GstOnImports,

        /// <summary>
        /// Historical Adjustment.
        /// </summary>
        [EnumMember(Value = "HISTORICAL")]
        Historical,

        /// <summary>
        /// Realised Currency Gains.
        /// </summary>
        [EnumMember(Value = "REALISEDCURRENCYGAIN")]
        RealisedCurrencyGain,

        /// <summary>
        /// Retained Earnings.
        /// </summary>
        [EnumMember(Value = "RETAINEDEARNINGS")]
        RetainedEarnings,

        /// <summary>
        /// Rounding.
        /// </summary>
        [EnumMember(Value = "ROUNDING")]
        Rounding,

        /// <summary>
        /// Tracking Transfers.
        /// </summary>
        [EnumMember(Value = "TRACKINGTRANSFERS")]
        TrackingTransfers,

        /// <summary>
        /// Unpaid Expense Claims.
        /// </summary>
        [EnumMember(Value = "UNPAIDEXPCLM")]
        UnpaidExpClm,

        /// <summary>
        /// Unrealised Currency Gains.
        /// </summary>
        [EnumMember(Value = "UNREALISEDCURRENCYGAIN")]
        UnrealisedCurrencyGain,

        /// <summary>
        /// Wages Payable.
        /// </summary>
        [EnumMember(Value = "WAGEPAYABLES")]
        WagePayables
    }
}
