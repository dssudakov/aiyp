﻿namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Address.
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Address type.
        /// </summary>
        public AddressType AddressType { get; set; }

        /// <summary>
        /// Address line 1.
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Address line 2.
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Address line 3.
        /// </summary>
        public string AddressLine3 { get; set; }

        /// <summary>
        /// Address line 4.
        /// </summary>
        public string AddressLine4 { get; set; }

        /// <summary>
        /// Address city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Address region.
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// Address postal code.
        /// </summary>
        public string PostalCode { get; set; }
        /// <summary>
        /// Address country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Attention to.
        /// </summary>
        public string AttentionTo { get; set; }
    }
}
