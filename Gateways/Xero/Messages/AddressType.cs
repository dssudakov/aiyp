﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Address type.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AddressType
    {
        /// <summary>
        /// The default mailing address for invoices.
        /// </summary>
        [EnumMember(Value = "POBOX")]
        POBox,

        /// <summary>
        /// Street address type.
        /// </summary>
        [EnumMember(Value = "STREET")]
        Street,

        /// <summary>
        /// Read-only via the GET Organisation endpoint (if set). 
        /// The delivery address of the Xero organisation. 
        /// DELIVERY address type is not valid for Contacts.
        /// </summary>
        [EnumMember(Value = "DELIVERY")]
        Delivery
    }
}
