﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Line amount types.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LineAmountTypes
    {
        /// <summary>
        /// Line items are exclusive of tax.
        /// </summary>
        [EnumMember(Value = "Exclusive")]
        Exclusive,

        /// <summary>
        /// Line items are inclusive tax.
        /// </summary>
        [EnumMember(Value = "Inclusive")]
        Inclusive,

        /// <summary>
        /// Line have no tax.
        /// </summary>
        [EnumMember(Value = "NoTax")]
        NoTax
    }
}
