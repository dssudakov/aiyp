﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Account class type.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AccountClassType
    {
        /// <summary>
        /// Asset.
        /// </summary>
        [EnumMember(Value = "ASSET")]
        Asset,

        /// <summary>
        /// Equity.
        /// </summary>
        [EnumMember(Value = "EQUITY")]
        Equity,

        /// <summary>
        /// Expense.
        /// </summary>
        [EnumMember(Value = "EXPENSE")]
        Expense,

        /// <summary>
        /// Liability.
        /// </summary>
        [EnumMember(Value = "LIABILITY")]
        Liability,

        /// <summary>
        /// Revenue.
        /// </summary>
        [EnumMember(Value = "REVENUE")]
        Revenue
    }
}
