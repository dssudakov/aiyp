﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Invoice.
    /// </summary>
    public class Invoice
    {
        /// <summary>
        /// Invoice type.
        /// </summary>
        public InvoiceType Type { get; set; }

        /// <summary>
        /// Invoice contact.
        /// </summary>
        public Contact Contact { get; set; }

        /// <summary>
        /// Date invoice was issued
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Date invoice is due.
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Invoice status code.
        /// </summary>
        public InvoiceStatus Status { get; set; }

        /// <summary>
        /// Line amount types.
        /// </summary>
        public LineAmountTypes LineAmountTypes { get; set; }

        /// <summary>
        /// invoice line items. The LineItems collection can contain any number of individual LineItem sub-elements.
        /// </summary>
        public IList<InvoiceLineItem> LineItems { get; set; }

        /// <summary>
        /// Total of invoice excluding taxes.
        /// </summary>
        public decimal SubTotal { get; set; }

        /// <summary>
        /// Total tax on invoice.
        /// </summary>
        public decimal TotalTax { get; set; }

        /// <summary>
        /// Total of Invoice tax inclusive.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Total of discounts applied on the invoice line items.
        /// </summary>
        public decimal TotalDiscount { get; set; }

        /// <summary>
        /// Last modified date UTC format.
        /// </summary>
        [JsonProperty("UpdatedDateUTC")]
        public DateTime UpdatedDateUtc { get; set; }

        /// <summary>
        /// The currency that invoice has been raised in.
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// The currency rate for a multicurrency invoice.
        /// </summary>
        public decimal CurrencyRate { get; set; }

        /// <summary>
        /// Xero generated unique identifier for invoice.
        /// </summary>
        [JsonProperty("InvoiceID")]
        public string InvoiceId { get; set; }

        /// <summary>
        /// ACCREC - Unique alpha numeric code identifying invoice.
        /// ACCPAY - Non-unique alpha numeric code identifying invoice. 
        /// This value will also display as Reference in the UI.
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// ACCREC only - additional reference number.
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// Invoice branding theme identifier.
        /// </summary>
        public string BrandingThemeID { get; set; }

        /// <summary>
        /// URL link to a source document - shown as "Go to [appName]" in the Xero app.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Boolean to indicate whether the invoice in the Xero app displays as "sent".
        /// </summary>
        public bool SentToContact { get; set; }

        /// <summary>
        /// Shown on sales invoices (Accounts Receivable) when this has been set.
        /// </summary>
        public DateTime? ExpectedPaymentDate { get; set; }

        /// <summary>
        /// Shown on bills (Accounts Payable) when this has been set.
        /// </summary>
        public DateTime? PlannedPaymentDate { get; set; }

        /// <summary>
        /// Boolean to indicate if an invoice has an attachment.
        /// </summary>
        public bool HasAttachments { get; set; }

        /// <summary>
        /// Amount remaining to be paid on invoice.
        /// </summary>
        public decimal AmountDue { get; set; }

        /// <summary>
        /// Sum of payments received for invoice.
        /// </summary>
        public decimal AmountPaid { get; set; }

        /// <summary>
        /// CISDeduction withheld by the contractor to be paid to HMRC on behalf of subcontractor 
        /// (Available for organisations under UK Construction Industry Scheme).
        /// </summary>
        public decimal? CISDeduction { get; set; }

        /// <summary>
        /// The date the invoice was fully paid. Only returned on fully paid invoices.
        /// </summary>
        public DateTime? FullyPaidOnDate { get; set; }

        /// <summary>
        /// Sum of all credit notes, over-payments and pre-payments applied to invoice.
        /// </summary>
        public decimal AmountCredited { get; set; }
    }
}
