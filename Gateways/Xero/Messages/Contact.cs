﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Xero contact object.
    /// </summary>
    public class Contact
    {
        /// <summary>
        /// Xero identifier.
        /// </summary>
        public Guid ContactID { get; set; }

        /// <summary>
        /// This field is read only in the Xero UI, used to identify contacts in external systems. 
        /// It is displayed as Contact Code in the Contacts UI in Xero.
        /// </summary>
        public string ContactNumber { get; set; }

        /// <summary>
        /// A user defined account number.
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Current status of a contact.
        /// </summary>
        public ContactStatus ContactStatus { get; set; }

        /// <summary>
        /// Full name of contact/organisation.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// First name of contact person.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of contact person.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email address of contact person.
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Skype user name of contact.
        /// </summary>
        public string SkypeUserName { get; set; }

        /// <summary>
        /// Bank account number of contact.
        /// </summary>
        public string BankAccountDetails { get; set; }

        /// <summary>
        /// Tax number of contact - this is also known as the 
        /// ABN (Australia), GST Number (New Zealand), VAT Number (UK) or Tax ID Number (US and global) 
        /// in the Xero UI depending on which regionalized version of Xero you are using.
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// Default tax type used for contact on AR invoices.
        /// </summary>
        public string AccountsReceivableTaxType { get; set; }

        /// <summary>
        /// Default tax type used for contact on AP invoices.
        /// </summary>
        public string AccountsPayableTaxType { get; set; }

        /// <summary>
        /// Store certain address types for a contact.
        /// </summary>
        public List<Address> Addresses { get; set; }

        /// <summary>
        /// Store certain phone types for a contact.
        /// </summary>
        public List<Phone> Phones { get; set; }

        /// <summary>
        /// True or false – Boolean that describes if a contact that has any AP invoices entered against them.
        /// </summary>
        public bool IsSupplier { get; set; }

        /// <summary>
        /// True or false – Boolean that describes if a contact has any AR invoices entered against them.
        /// </summary>
        public bool IsCustomer { get; set; }

        /// <summary>
        /// Default currency for raising invoices against contact.
        /// </summary>
        public string DefaultCurrency { get; set; }

        /// <summary>
        /// UTC timestamp of last update to contact.
        /// </summary>
        [JsonProperty("UpdatedDateUTC")]
        public DateTime? UpdatedDateUtc { get; set; }
    }
}
