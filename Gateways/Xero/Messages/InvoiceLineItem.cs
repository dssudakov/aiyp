﻿using System;
using System.Collections.Generic;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Invoice line item.
    /// </summary>
    public class InvoiceLineItem
    {
        /// <summary>
        /// The description of the line item.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// LineItem Quantity.
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// Lineitem unit amount. By default, unit amount will be rounded to two decimal places. 
        /// You can opt in to use four decimal places by adding the querystring parameter unitdp=4 to your query. 
        /// </summary>
        public decimal UnitAmount { get; set; }

        /// <summary>
        /// Line item code.
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        /// Line item account code.
        /// </summary>
        public string AccountCode { get; set; }

        /// <summary>
        /// The Xero generated identifier for a LineItem.
        /// </summary>
        public Guid LineItemID { get; set; }

        /// <summary>
        /// Used as an override if the default Tax Code for the selected AccountCode is not correct.
        /// </summary>
        public string TaxType { get; set; }

        /// <summary>
        /// The tax amount is auto calculated as a percentage of the line amount based on the tax rate.
        /// </summary>
        public decimal TaxAmount { get; set; }

        /// <summary>
        /// The line amount reflects the discounted price if a DiscountRate has been used 
        /// i.e LineAmount = Quantity * Unit Amount * ((100 - DiscountRate)/100).
        /// </summary>
        public decimal LineAmount { get; set; }

        /// <summary>
        /// Percentage discount being applied to a line item 
        /// (only supported on ACCREC invoices - ACC PAY invoices and credit notes in Xero do not support discounts).
        /// </summary>
        public decimal? DiscountRate { get; set; }

        /// <summary>
        /// Section for optional Tracking Category.
        /// Any LineItem can have a maximum of 2 TrackingCategory elements.
        /// </summary>
        public IList<TrackingCategory> Tracking { get; set; }
    }
}
