﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Account type.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AccountType
    {
        /// <summary>
        /// Bank account.
        /// </summary>
        [EnumMember(Value = "BANK")]
        Bank,

        /// <summary>
        /// Current Asset account.
        /// </summary>
        [EnumMember(Value = "CURRENT")]
        Current,

        /// <summary>
        /// Current Liability account.
        /// </summary>
        [EnumMember(Value = "CURRLIAB")]
        CurrLiab,

        /// <summary>
        /// Depreciation account.
        /// </summary>
        [EnumMember(Value = "DEPRECIATN")]
        Depreciatn,

        /// <summary>
        /// Direct Costs account.
        /// </summary>
        [EnumMember(Value = "DIRECTCOSTS")]
        DirectCosts,

        /// <summary>
        /// Equity account.
        /// </summary>
        [EnumMember(Value = "EQUITY")]
        Equity,

        /// <summary>
        /// Expense account.
        /// </summary>
        [EnumMember(Value = "EXPENSE")]
        Expense,

        /// <summary>
        /// Fixed Asset account.
        /// </summary>
        [EnumMember(Value = "FIXED")]
        Fixed,

        /// <summary>
        /// Inventory Asset account.
        /// </summary>
        [EnumMember(Value = "INVENTORY")]
        Inventory,

        /// <summary>
        /// Liability account.
        /// </summary>
        [EnumMember(Value = "LIABILITY")]
        Liability,

        /// <summary>
        /// Non-current Asset account.
        /// </summary>
        [EnumMember(Value = "NONCURRENT")]
        NonCurrent,

        /// <summary>
        /// Other Income account.
        /// </summary>
        [EnumMember(Value = "OTHERINCOME")]
        OtherIncome,

        /// <summary>
        /// Overhead account.
        /// </summary>
        [EnumMember(Value = "OVERHEADS")]
        Overheads,

        /// <summary>
        /// Prepayment account.
        /// </summary>
        [EnumMember(Value = "PREPAYMENT")]
        Prepayment,

        /// <summary>
        /// Revenue account.
        /// </summary>
        [EnumMember(Value = "REVENUE")]
        Revenue,

        /// <summary>
        /// Sale account.
        /// </summary>
        [EnumMember(Value = "SALES")]
        Sales,

        /// <summary>
        /// Non-current Liability account.
        /// </summary>
        [EnumMember(Value = "TERMLIAB")]
        TermLiab,

        /// <summary>
        /// PAYG Liability account.
        /// </summary>
        [EnumMember(Value = "PAYGLIABILITY")]
        PaygLiability,

        /// <summary>
        /// Superannuation Expense account.
        /// </summary>
        [EnumMember(Value = "SUPERANNUATIONEXPENSE")]
        SuperAnnuationExpense,

        /// <summary>
        /// Superannuation Liability account.
        /// </summary>
        [EnumMember(Value = "SUPERANNUATIONLIABILITY")]
        SuperAnnuationLiability,

        /// <summary>
        /// Wages Expense account.
        /// </summary>
        [EnumMember(Value = "WAGESEXPENSE")]
        WagesExpense
    }
}
