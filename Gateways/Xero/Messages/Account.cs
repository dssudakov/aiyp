﻿using Newtonsoft.Json;
using System;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Account.
    /// </summary>
    public class Account
    {
        /// <summary>
        /// Customer defined alpha numeric account code e.g 200 or SALES.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Name of account.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Account type.
        /// </summary>
        public AccountType Type { get; set; }

        /// <summary>
        /// For bank accounts only (Account Type BANK).
        /// </summary>
        public string BankAccountNumber { get; set; }

        /// <summary>
        /// Accounts with a status of ACTIVE can be updated to ARCHIVED.
        /// </summary>
        public AccountStatusCode Status { get; set; }

        /// <summary>
        /// Description of the Account. Valid for all types of accounts except bank accounts.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Bank account type. For bank accounts only.
        /// </summary>
        public BankAccountType? BankAccountType { get; set; }

        /// <summary>
        /// Currency code. For bank accounts only.
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Tax type.
        /// </summary>
        public string TaxType { get; set; }

        /// <summary>
        /// Boolean - describes whether account can have payments applied to it.
        /// </summary>
        public bool EnablePaymentsToAccount { get; set; }

        /// <summary>
        /// Boolean - describes whether account code is available for use with expense claims.
        /// </summary>
        public bool? ShowInExpenseClaims { get; set; }

        /// <summary>
        /// Xero identifier.
        /// </summary>
        public Guid AccountID { get; set; }

        /// <summary>
        /// Account class type.
        /// </summary>
        public AccountClassType Class { get; set; }

        /// <summary>
        /// If this is a system account then this element is returned.
        /// </summary>
        public SystemAccountType? SystemAccount { get; set; }

        /// <summary>
        /// Shown if set.
        /// </summary>
        public string ReportingCode { get; set; }

        /// <summary>
        /// Shown if set.
        /// </summary>
        public string ReportingCodeName { get; set; }

        /// <summary>
        /// boolean to indicate if an account has an attachment.
        /// </summary>
        public bool HasAttachments { get; set; }

        /// <summary>
        /// Last modified date UTC format.
        /// </summary>
        [JsonProperty("UpdatedDateUTC")]
        public DateTime UpdatedDateUtc { get; set; }
    }
}
