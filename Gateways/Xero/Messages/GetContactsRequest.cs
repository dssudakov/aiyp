﻿namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Get contacts request.
    /// </summary>
    public class GetContactsRequest : GetRequestBase
    {
        /// <summary>
        /// Endpoint.
        /// </summary>
        public override string Endpoint => "Contacts";
    }
}
