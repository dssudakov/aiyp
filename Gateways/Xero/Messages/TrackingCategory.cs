﻿using System;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Tracking category.
    /// </summary>
    public class TrackingCategory
    {
        /// <summary>
        /// Name of the tracking category.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Xero assigned unique ID for the category.
        /// </summary>
        public Guid TrackingCategoryID { get; set; }

        /// <summary>
        /// Name of the option (required).
        /// </summary>
        public string Option { get; set; }
    }
}
