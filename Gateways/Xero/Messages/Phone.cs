﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Phone.
    /// </summary>
    public class Phone
    {
        /// <summary>
        /// Phone type.
        /// </summary>
        public string PhoneType { get; set; }
        
        /// <summary>
        /// Phone number.
        /// </summary>
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// Phone area code.
        /// </summary>
        public string PhoneAreaCode { get; set; }

        /// <summary>
        /// Phone country code.
        /// </summary>
        public string PhoneCountryCode { get; set; }
    }
}
