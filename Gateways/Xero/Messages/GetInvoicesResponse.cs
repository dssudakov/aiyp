﻿using System.Collections.Generic;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Get invoices response.
    /// </summary>
    public class GetInvoicesResponse
    {
        /// <summary>
        /// Invoices collection.
        /// </summary>
        public IList<Invoice> Invoices { get; set; }
    }
}
