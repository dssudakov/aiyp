﻿namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Get invoices request.
    /// </summary>
    public class GetInvoicesRequest : GetRequestBase
    {
        /// <summary>
        /// Endpoint.
        /// </summary>
        public override string Endpoint => "Invoices";
    }
}
