﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Phone type.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum PhoneType
    {
        /// <summary>
        /// Default.
        /// </summary>
        [EnumMember(Value = "ACTIVE")]
        Default,

        /// <summary>
        /// DDI.
        /// </summary>
        [EnumMember(Value = "ACTIVE")]
        Ddi,

        /// <summary>
        /// Mobile.
        /// </summary>
        [EnumMember(Value = "ACTIVE")]
        Mobile,

        /// <summary>
        /// Fax.
        /// </summary>
        [EnumMember(Value = "ACTIVE")]
        Fax
    }
}
