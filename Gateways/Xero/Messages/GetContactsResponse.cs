﻿using System.Collections.Generic;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Get contacts response.
    /// </summary>
    public class GetContactsResponse
    {
        /// <summary>
        /// Contacts collection.
        /// </summary>
        public IList<Contact> Contacts { get; set; }
    }
}
