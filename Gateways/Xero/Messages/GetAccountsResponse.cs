﻿using System.Collections.Generic;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Get accounts response.
    /// </summary>
    public class GetAccountsResponse
    {
        /// <summary>
        /// Accounts collection.
        /// </summary>
        public IList<Account> Accounts { get; set; }
    }
}
