﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AIYP.Gateways.Xero.Messages
{
    /// <summary>
    /// Bank account type.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum BankAccountType
    {
        /// <summary>
        /// None.
        /// </summary>
        [EnumMember(Value = "NONE")]
        None,

        /// <summary>
        /// Bank.
        /// </summary>
        [EnumMember(Value = "BANK")]
        Bank,

        /// <summary>
        /// Credit card.
        /// </summary>
        [EnumMember(Value = "CREDITCARD")]
        CreditCard,

        /// <summary>
        /// PayPal.
        /// </summary>
        [EnumMember(Value = "PAYPAL")]
        PayPal
    }
}
