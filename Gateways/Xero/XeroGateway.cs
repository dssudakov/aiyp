﻿using AIYP.Gateways.Common;
using AIYP.Gateways.Xero.Messages;
using AIYP.Gateways.Xero.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AIYP.Gateways.Xero
{
    /// <summary>
    /// Xero gateway.
    /// </summary>
    public class XeroGateway : IXeroGateway
    {
        private readonly IAuthorizationHeaderFactory _authorizationHeaderFactory;
        private readonly XeroGatewaySettings _settings;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="authorizationHeaderFactory">Authorization header factory.</param>
        /// <param name="settings">Settings.</param>
        public XeroGateway(IAuthorizationHeaderFactory authorizationHeaderFactory, XeroGatewaySettings settings)
        {
            _authorizationHeaderFactory = authorizationHeaderFactory;
            _settings = settings;
        }

        /// <summary>
        /// Returns accounts response.
        /// </summary>
        /// <param name="request">Accounts request.</param>
        /// <returns>Accounts response.</returns>
        public Task<GetAccountsResponse> SendAsync(GetAccountsRequest request)
        {
            return GetAsync<GetAccountsRequest, GetAccountsResponse>(request);
        }

        /// <summary>
        /// Returns contacts response.
        /// </summary>
        /// <param name="request">Contacts request.</param>
        /// <returns>Contacts response.</returns>
        public Task<GetContactsResponse> SendAsync(GetContactsRequest request)
        {
            return GetAsync<GetContactsRequest, GetContactsResponse>(request);
        }

        /// <summary>
        /// Returns invoices response.
        /// </summary>
        /// <param name="request">Invoices request.</param>
        /// <returns>Invoices response.</returns>
        public Task<GetInvoicesResponse> SendAsync(GetInvoicesRequest request)
        {
            return GetAsync<GetInvoicesRequest, GetInvoicesResponse>(request);
        }

        private async Task<TResponse> GetAsync<TRequest, TResponse>(TRequest request)
            where TRequest: GetRequestBase
        {
            var uri = CreateUriFor(request);
            var authHeader = MakeAuthHeader("GET", uri);
            var headers = new Dictionary<string, string>()
            {
                { "Authorization", authHeader },
                { "Accept", "application/json" }
            };

            var responseText = await new GatewayHttpService(uri, headers).GetAsync();
            var response = JsonConvert.DeserializeObject<TResponse>(responseText);

            return response;
        }

        private string MakeAuthHeader(string method, string uri)
        {
            switch(_settings.ApplicationType)
            {
                case XeroApplicationType.Private:
                    return _authorizationHeaderFactory.MakeHeader(
                        method, uri, _settings.ConsumerKey, _settings.ConsumerKey);
                case XeroApplicationType.Public:
                    return _authorizationHeaderFactory.MakeHeader(
                        method, uri, _settings.ConsumerKey, _settings.TokenKey);
                default:
                    throw new InvalidOperationException("Invalid application type.");
            }
        }

        private string CreateUriFor(GetRequestBase request)
        {
            var uriBuilder = new UriBuilder(new Uri(new Uri(this._settings.BaseUrl), request.Endpoint));
            uriBuilder.Query = request.Query;

            var uri = uriBuilder.ToString();
            return uri;
        }
    }
}
