﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace AIYP.Gateways.Xero.Services
{
    /// <summary>
    /// Private signature service.
    /// </summary>
    public class PrivateSignatureService : ISignatureService
    {
        private readonly X509Certificate2 certificate;

        /// <summary>
        /// Returns signature method.
        /// </summary>
        public string SignatureMethod => "RSA-SHA1";

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="certificate">Certificate.</param>
        public PrivateSignatureService(X509Certificate2 certificate)
        {
            this.certificate = certificate;
        }

        /// <summary>
        /// Returns signature.
        /// </summary>
        /// <param name="data">Data to sign.</param>
        /// <returns>Signature.</returns>
        public string CreateSignature(string data)
        {
            using (var hashAlgorithm = SHA1.Create())
            {
                var dataBytes = Encoding.ASCII.GetBytes(data);
                var hashBytes = hashAlgorithm.ComputeHash(dataBytes);

                var signatureBytes = this.certificate
                    .GetRSAPrivateKey()
                    .SignHash(hashBytes, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);

                var signature = Convert.ToBase64String(signatureBytes);

                return signature;
            }
        }
    }
}
