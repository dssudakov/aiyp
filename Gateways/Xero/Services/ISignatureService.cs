﻿namespace AIYP.Gateways.Xero.Services
{
    /// <summary>
    /// Signature service interface.
    /// </summary>
    public interface ISignatureService
    {
        /// <summary>
        /// Returns signature method.
        /// </summary>
        string SignatureMethod { get; }

        /// <summary>
        /// Returns signature.
        /// </summary>
        /// <param name="data">Data to sign.</param>
        /// <returns>Signature.</returns>
        string CreateSignature(string data);
    }
}
