﻿namespace AIYP.Gateways.Xero.Services
{
    /// <summary>
    /// Authorization factory interface.
    /// </summary>
    public interface IAuthorizationHeaderFactory
    {
        /// <summary>
        /// Returns authorization header.
        /// </summary>
        /// <param name="method">Method.</param>
        /// <param name="uri">URI</param>
        /// <param name="consumerKey">Consumer key.</param>
        /// <param name="tokenKey">Token key.</param>
        /// <returns>Authorization header.</returns>
        string MakeHeader(string method, string uri, string consumerKey, string tokenKey);
    }
}
