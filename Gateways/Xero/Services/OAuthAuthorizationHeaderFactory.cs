﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AIYP.Gateways.Xero.Services
{
    /// <summary>
    /// Authorization factory.
    /// </summary>
    public class OAuthAuthorizationHeaderFactory : IAuthorizationHeaderFactory
    {
        private readonly ISignatureService _signatureService;
        private readonly INonceProvider _nonceProvider;
        private readonly ITimestampProvider _timestampProvider;

        public OAuthAuthorizationHeaderFactory(
            ISignatureService signatureService,
            INonceProvider nonceProvider,
            ITimestampProvider timestampProvider)
        {
            _signatureService = signatureService;
            _nonceProvider = nonceProvider;
            _timestampProvider = timestampProvider;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="signatureService">Signature service.</param>
        public OAuthAuthorizationHeaderFactory(ISignatureService signatureService)
            : this(signatureService, new GuidNonceProvider(), new UnixEpocTimestampProvider())
        {
        }

        /// <summary>
        /// Returns authorization header.
        /// </summary>
        /// <param name="method">Method.</param>
        /// <param name="uri">URI</param>
        /// <param name="consumerKey">Consumer key.</param>
        /// <param name="tokenKey">Token key.</param>
        /// <returns>Authorization header.</returns>
        public string MakeHeader(string method, string uri, string consumerKey, string tokenKey)
        {
            if (string.IsNullOrEmpty(uri))
            {
                throw new ArgumentNullException(nameof(uri));
            }

            var parameters = new Dictionary<string, string>();
            parameters.Add("oauth_consumer_key", consumerKey);
            parameters.Add("oauth_token", tokenKey);
            parameters.Add("oauth_signature_method", _signatureService.SignatureMethod);
            parameters.Add("oauth_signature", null);
            parameters.Add("oauth_timestamp", _timestampProvider.GetTimestamp());
            parameters.Add("oauth_nonce", _nonceProvider.GetNonce());
            parameters.Add("oauth_version", "1.0");

            var endpoint = new Uri(uri).GetLeftPart(UriPartial.Path);
            var query = new Uri(uri).Query?.TrimStart('?');
            var dataToSign = GetDataToSign(method, endpoint, parameters, query);

            var signature = _signatureService.CreateSignature(dataToSign);
            parameters["oauth_signature"] = Escape(signature);

            return string.Format("OAuth {0}", GetParametersStringForHeader(parameters));
        }

        private string GetDataToSign(
            string method, string endpoint, IDictionary<string, string> parameters, string query)
        {
            var parametersString = GetParametersStringForSignatureData(parameters);
            if (!string.IsNullOrEmpty(query))
            {
                parametersString = string.Join("&", parametersString, query);
            }

            return string.Format(
                "{0}&{1}&{2}", Escape(method), Escape(endpoint), Escape(parametersString));
        }

        private string GetParametersStringForSignatureData(IDictionary<string, string> parameters)
        {
            var pairs = parameters
                .OrderBy(x => x.Key)
                .Where(x => !string.IsNullOrEmpty(x.Value))
                .Select(x => string.Format("{0}={1}", x.Key, x.Value));

            return string.Join("&", pairs);
        }

        private string GetParametersStringForHeader(IDictionary<string, string> parameters)
        {
            var pairs = parameters
                .Where(x => !string.IsNullOrEmpty(x.Value))
                .Select(x => string.Format("{0}=\"{1}\"", x.Key, x.Value));

            return string.Join(", ", pairs);
        }

        private string Escape(string data)
        {
            return Uri.EscapeDataString(data ?? string.Empty);
        }
    }
}
