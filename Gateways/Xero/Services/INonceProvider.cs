﻿namespace AIYP.Gateways.Xero.Services
{
    /// <summary>
    /// Nonce provider interface.
    /// </summary>
    public interface INonceProvider
    {
        /// <summary>
        /// Returns nonce.
        /// </summary>
        /// <returns>Nonce.</returns>
        string GetNonce();
    }
}
