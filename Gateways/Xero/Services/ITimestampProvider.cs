﻿namespace AIYP.Gateways.Xero.Services
{
    /// <summary>
    /// Timestamp provider interface.
    /// </summary>
    public interface ITimestampProvider
    {
        /// <summary>
        /// Returns timestamp.
        /// </summary>
        /// <returns>Timestamp.</returns>
        string GetTimestamp();
    }
}
