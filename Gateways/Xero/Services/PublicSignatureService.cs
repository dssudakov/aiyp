﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace AIYP.Gateways.Xero.Services
{
    /// <summary>
    /// Public signature service.
    /// </summary>
    public class PublicSignatureService : ISignatureService
    {
        private readonly string consumerSecret;
        private readonly string tokenSecret;

        public string SignatureMethod => "HMAC-SHA1";

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="consumerSecret">Consumer secret.</param>
        /// <param name="tokenSecret">Token secret.</param>
        public PublicSignatureService(string consumerSecret, string tokenSecret)
        {
            this.consumerSecret = consumerSecret;
            this.tokenSecret = tokenSecret;
        }

        /// <summary>
        /// Returns signature.
        /// </summary>
        /// <param name="data">Data to sign.</param>
        /// <returns>Signature.</returns>
        public string CreateSignature(string data)
        {
            var key = string.Format("{0}&{1}", this.consumerSecret, this.tokenSecret);

            var keyBytes = Encoding.ASCII.GetBytes(key);
            var dataBytes = Encoding.ASCII.GetBytes(data);

            var signatureBytes = CreateSignatureBytes(keyBytes, dataBytes);
            var signature = Convert.ToBase64String(signatureBytes);

            return signature;
        }

        private static byte[] CreateSignatureBytes(byte[] keyBytes, byte[] dataBytes)
        {
            using (var algorithm = new HMACSHA1(keyBytes))
            {
                return algorithm.ComputeHash(dataBytes);
            }
        }
    }
}
