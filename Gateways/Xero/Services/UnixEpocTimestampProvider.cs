﻿using System;

namespace AIYP.Gateways.Xero.Services
{
    /// <summary>
    /// Timestamp provider.
    /// </summary>
    public class UnixEpocTimestampProvider : ITimestampProvider
    {
        /// <summary>
        /// Returns timestamp.
        /// </summary>
        /// <returns>Timestamp.</returns>
        public string GetTimestamp()
        {
            TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1);
            int secondsSinceEpoch = (int)timeSpan.TotalSeconds;

            return secondsSinceEpoch.ToString();
        }
    }
}
