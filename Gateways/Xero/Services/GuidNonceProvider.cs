﻿using System;

namespace AIYP.Gateways.Xero.Services
{
    /// <summary>
    /// Guid nonce provider.
    /// </summary>
    public class GuidNonceProvider : INonceProvider
    {
        /// <summary>
        /// Returns nonce.
        /// </summary>
        /// <returns>Nonce.</returns>
        public string GetNonce()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
