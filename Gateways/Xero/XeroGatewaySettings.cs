﻿namespace AIYP.Gateways.Xero
{
    /// <summary>
    /// Xero gateway settings.
    /// </summary>
    public class XeroGatewaySettings
    {
        /// <summary>
        /// Application type.
        /// </summary>
        public XeroApplicationType ApplicationType { get; set; }

        /// <summary>
        /// Base URL.
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// Certificate path.
        /// </summary>
        public string CertificatePath { get; set; }

        /// <summary>
        /// Certificate password.
        /// </summary>
        public string CertificatePassword { get; set; }

        /// <summary>
        /// Consumer key.
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// Consumer secret.
        /// </summary>
        public string ConsumerSecret { get; set; }

        /// <summary>
        /// Token key.
        /// </summary>
        public string TokenKey { get; set; }

        /// <summary>
        /// Token secret.
        /// </summary>
        public string TokenSecret { get; set; }
    }
}
