﻿using AIYP.Gateways.Xero.Messages;
using System.Threading.Tasks;

namespace AIYP.Gateways.Xero
{
    /// <summary>
    /// Xero gateway interface.
    /// </summary>
    public interface IXeroGateway
    {
        /// <summary>
        /// Returns accounts response.
        /// </summary>
        /// <param name="request">Accounts request.</param>
        /// <returns>Accounts response.</returns>
        Task<GetAccountsResponse> SendAsync(GetAccountsRequest request);

        /// <summary>
        /// Returns contacts response.
        /// </summary>
        /// <param name="request">Contacts request.</param>
        /// <returns>Contacts response.</returns>
        Task<GetContactsResponse> SendAsync(GetContactsRequest request);

        /// <summary>
        /// Returns invoices response.
        /// </summary>
        /// <param name="request">Invoices request.</param>
        /// <returns>Invoices response.</returns>
        Task<GetInvoicesResponse> SendAsync(GetInvoicesRequest request);
    }
}
