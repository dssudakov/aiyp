﻿namespace AIYP.Gateways.Xero
{
    /// <summary>
    /// Xero application type.
    /// </summary>
    public enum XeroApplicationType
    {
        /// <summary>
        /// Private.
        /// </summary>
        Private = 1,

        /// <summary>
        /// Public.
        /// </summary>
        Public = 2
    }
}
