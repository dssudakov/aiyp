﻿using AIYP.Gateways.Xero.Services;
using FluentAssertions;
using NUnit.Framework;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace AIYP.Gateways.Tests.Xero
{
    [TestFixture]
    public class PrivateSignatureServiceTests
    {
        private const string CERTIFICATE_PATH = "Xero\\public_privatekey.pfx";

        private ISignatureService signatureService;

        [SetUp]
        public void SetUp()
        {
            var fullCertificatePath = Path.Combine(TestContext.CurrentContext.TestDirectory, CERTIFICATE_PATH);
            var certificate = new X509Certificate2(
                fullCertificatePath, string.Empty, X509KeyStorageFlags.MachineKeySet);

            this.signatureService = new PrivateSignatureService(certificate);
        }

        [Test]
        public void CreateSignature_CreatesExpectedSignature()
        {
            var data = "GET&https%3A%2F%2Fapi.xero.com%2Fapi.xro%2F2.0%2FOrganisation&oauth_consumer_key%3DSH4OP7L0FQWGYB5JO4Q1IIGRD5LEUJ%26oauth_nonce%3D8fe1cff7-df0b-4a49-9dad-2096e4233fce%26oauth_signature_method%3DRSA-SHA1%26oauth_timestamp%3D1550561567%26oauth_token%3DSH4OP7L0FQWGYB5JO4Q1IIGRD5LEUJ%26oauth_version%3D1.0";
            var expectedSignature = "SOM6qVDvIqav0q2DG0MOa64cyF1T74fyvK4tvYNG/EnRlf9wayozNMkQ5ai9nWZxSzRNKGx9HRGQbJzRGNkkiIoQk10rYJgY/zT+u/XCnThlnIklIj9wWtE1sZBZuF9NRaNoIsbveKI7RUzTqB5yZZtXmjGGzStzu2sbMJo7OP0=";

            var signature = this.signatureService.CreateSignature(data);
            signature.Should().Be(expectedSignature);
        }

        [Test]
        public void SignatureMethod_ReturnsRsaHsa1()
        {
            this.signatureService.SignatureMethod.Should().Be("RSA-SHA1");
        }
    }
}
