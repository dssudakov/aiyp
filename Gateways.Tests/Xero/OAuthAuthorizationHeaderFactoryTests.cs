﻿using AIYP.Gateways.Xero.Services;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace AIYP.Gateways.Tests.Xero
{
    [TestFixture]
    public class OAuthAuthorizationHeaderFactoryTests
    {
        private Mock<ISignatureService> _signatureServiceMock;

        private IAuthorizationHeaderFactory _headerFactory;

        [SetUp]
        public void SetUp()
        {
            _signatureServiceMock = new Mock<ISignatureService>();
            _signatureServiceMock.Setup(x => x.SignatureMethod).Returns("HMAC-SHA1");

            var nonceProviderMock = new Mock<INonceProvider>();
            nonceProviderMock.Setup(x => x.GetNonce()).Returns("899c8740-59d1-4d48-8104-2653e9225d51");

            var timestampProviderMock = new Mock<ITimestampProvider>();
            timestampProviderMock.Setup(x => x.GetTimestamp()).Returns("1550322358");

            _headerFactory = new OAuthAuthorizationHeaderFactory(
                _signatureServiceMock.Object, nonceProviderMock.Object, timestampProviderMock.Object);
        }

        [Test]
        public void MakeHeader_ReturnsCorrectHeader()
        {
            _signatureServiceMock
                .Setup(x => x.CreateSignature(It.IsAny<string>()))
                .Returns("vgQWxHtSKqD/zcVaH0Yeuokw/oM=");

            var header = _headerFactory.MakeHeader(
                "GET",
                "https://api.xero.com/api.xro/2.0/Organisation",
                "D0FZVXMCPZT0K71J4HOQVIJT1WV6ZK",
                "VEVAURW6APTN9EBOWJ95UPWXIFX3V5");

            header.Should().Be(
                "OAuth " +
                "oauth_consumer_key=\"D0FZVXMCPZT0K71J4HOQVIJT1WV6ZK\", " +
                "oauth_token=\"VEVAURW6APTN9EBOWJ95UPWXIFX3V5\", " +
                "oauth_signature_method=\"HMAC-SHA1\", " +
                "oauth_signature=\"vgQWxHtSKqD%2FzcVaH0Yeuokw%2FoM%3D\", " +
                "oauth_timestamp=\"1550322358\", " +
                "oauth_nonce=\"899c8740-59d1-4d48-8104-2653e9225d51\", " +
                "oauth_version=\"1.0\"");
        }

        [Test]
        public void MakeHeader_FormsCorrectDataForSignature()
        {
            var expectedDataForSignature =
                "GET&" +
                "https%3A%2F%2Fapi.xero.com%2Fapi.xro%2F2.0%2FOrganisation&" +
                "oauth_consumer_key%3DD0FZVXMCPZT0K71J4HOQVIJT1WV6ZK%26" +
                "oauth_nonce%3D899c8740-59d1-4d48-8104-2653e9225d51%26" +
                "oauth_signature_method%3DHMAC-SHA1%26" +
                "oauth_timestamp%3D1550322358%26" +
                "oauth_token%3DVEVAURW6APTN9EBOWJ95UPWXIFX3V5%26" +
                "oauth_version%3D1.0";

            _headerFactory.MakeHeader(
                "GET",
                "https://api.xero.com/api.xro/2.0/Organisation",
                "D0FZVXMCPZT0K71J4HOQVIJT1WV6ZK",
                "VEVAURW6APTN9EBOWJ95UPWXIFX3V5");
            _signatureServiceMock.Verify(x => x.CreateSignature(expectedDataForSignature));
        }
    }
}
