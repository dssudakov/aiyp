﻿using AIYP.Gateways.Xero;
using AIYP.Gateways.Xero.Messages;
using AIYP.Gateways.Xero.Services;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AIYP.Gateways.Tests.Xero
{
    [TestFixture]
    public class XeroGatewayTests
    {
        private IXeroGateway _xeroGateway;

        [SetUp]
        public void SetUp()
        {
            var certificatePath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Xero\\public_privatekey.pfx");
            var certificate = new X509Certificate2(certificatePath, string.Empty, X509KeyStorageFlags.MachineKeySet);

            var settings = new XeroGatewaySettings
            {
                ApplicationType = XeroApplicationType.Private,
                BaseUrl = "https://api.xero.com/api.xro/2.0/",
                CertificatePath = certificatePath,
                ConsumerKey = "SH4OP7L0FQWGYB5JO4Q1IIGRD5LEUJ",
                ConsumerSecret = "WKL8XR8E9TGOVOGQFZPWBIDXFPQ2LZ",
                TokenKey = "SALEJMB8HPH8M1QVGKSSA2BQVDAYBH",
                TokenSecret = "ISHALZXUYIAWRLTNZ7PHB5HT6C6O4G"
            };

            var signatureService = new PrivateSignatureService(certificate);
            var authHeaderFactory = new OAuthAuthorizationHeaderFactory(signatureService);

            _xeroGateway = new XeroGateway(authHeaderFactory, settings);
        }

        [Test]
        public async Task GetAccounts_ReturnsAccounts()
        {
            var request = new GetAccountsRequest
            {
                StartDate = DateTime.UtcNow.Date.AddDays(-50),
                EndDate = DateTime.UtcNow.Date.AddDays(1)
            };

            var response = await _xeroGateway.SendAsync(request);

            response.Should().NotBeNull();
            response.Accounts.Should().NotBeNullOrEmpty();
        }

        [Test]
        public async Task GetContacts_ReturnsContacts()
        {
            var response = await _xeroGateway.SendAsync(new GetContactsRequest());
            response.Should().NotBeNull();
            response.Contacts.Should().NotBeNullOrEmpty();
        }

        [Test]
        public async Task GetInvoices_ReturnsInvoices()
        {
            var response = await _xeroGateway.SendAsync(new GetInvoicesRequest());
            response.Should().NotBeNull();
            response.Invoices.Should().NotBeNullOrEmpty();
        }
    }
}
