﻿using AIYP.Gateways.Xero.Services;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AIYP.Gateways.Tests.Xero
{
    [TestFixture]
    public class PublicSignatureServiceTests
    {
        private const string CONSUMER_SECRET = "WKL8XR8E9TGOVOGQFZPWBIDXFPQ2LZ";
        private const string TOKEN_SECRET = "2IERX9BEROIUXCG0O9MYLASW7QV4U7";

        private ISignatureService signatureService;

        [SetUp]
        public void SetUp()
        {
            this.signatureService = new PublicSignatureService(CONSUMER_SECRET, TOKEN_SECRET);
        }

        [Test]
        public void CreateSignature_CreatesExpectedSignature()
        {
            var data = "GET&https%3A%2F%2Fapi.xero.com%2Fapi.xro%2F2.0%2FOrganisation&oauth_consumer_key%3DD0FZVXMCPZT0K71J4HOQVIJT1WV6ZK%26oauth_nonce%3D899c8740-59d1-4d48-8104-2653e9225d51%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1550322358%26oauth_token%3DVEVAURW6APTN9EBOWJ95UPWXIFX3V5%26oauth_version%3D1.0";
            var expectedSignature = "vgQWxHtSKqD/zcVaH0Yeuokw/oM=";

            var signature = this.signatureService.CreateSignature(data);
            signature.Should().Be(expectedSignature);
        }

        [Test]
        public void SignatureMethod_ReturnsHmacHsa1()
        {
            this.signatureService.SignatureMethod.Should().Be("HMAC-SHA1");
        }
    }
}
